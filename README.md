# QWC2 Tools - QGIS Plugin

[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![Imports: isort](https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336)](https://pycqa.github.io/isort/)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)
[![flake8](https://img.shields.io/badge/linter-flake8-green)](https://flake8.pycqa.org/)

## Description

A QGIS plugin for QGIS Web Client (QWC)

### QGIS

[QGIS](https://www.qgis.org/en/site/) is a full-featured, user-friendly, free-and-open-source (FOSS) geographical information system (GIS) that runs on Unix platforms, Windows, and MacOS.

### QWC

[QGIS Web Client](https://qwc-services.github.io/master/) (QWC) is a modular next generation responsive web client for QGIS Server, built with ReactJS and OpenLayers. The core concept of QWC is to display QGIS Projects which are published by QGIS Server via WMS.

### QWC Tools - QGIS plugin

QWC Tools is a QGIS plugin that lets you manage, publish, update and manage projects for your QWC instance directly from the QGIS interface. All with an ergonomic explorer integrated into QGIS.

You can publish your project directly from the QGIS interface.

![img](docs/img/readme_publish_project.gif)

Then you can retrieve this project to create a theme and display it in QWC from the QWC administration interface :

![admin](./docs/img/qwc_admin.png)
![admin](./docs/img/qwc_themes.png)

### Prerequisites

- A functional QWC instance
- In this instance, add the **publishing service** required to run the plugin : [QWC Publish Service](https://gitlab.com/Oslandia/qwc/qwc-publish-service) (:warning: **Without this service, it is not possible to use the plugin** )

### Documentation

🇬🇧 [Check-out the documentation](https://oslandia.gitlab.io/qgis/qwc2_tools/)

## Credits

This plugin has been developed by Oslandia ( <https://oslandia.com> ).

Oslandia provides support and assistance for QGIS and associated tools, including this plugin.

This initial work has been funded by **Les Agences de l'eau** | Direction des Systèmes d’Information et des Usages Numériques ( <https://www.lesagencesdeleau.fr/> ) ![Agence de l'eau logo](docs/img/logo_agence_eau.svg).

## License

Distributed under the terms of the [`GPLv2+` license](LICENSE).
