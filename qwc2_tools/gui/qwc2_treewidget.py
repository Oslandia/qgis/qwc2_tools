import json

from qgis.core import Qgis, QgsApplication
from qgis.PyQt.Qt import QUrl
from qgis.PyQt.QtCore import pyqtSignal
from qgis.PyQt.QtGui import QFont, QIcon
from qgis.PyQt.QtWidgets import QTreeWidget, QTreeWidgetItem

from qwc2_tools.toolbelt import PlgLogger
from qwc2_tools.toolbelt.network_manager import NetworkRequestsManager
from qwc2_tools.toolbelt.preferences import PlgOptionsManager, Tenant

FILE = 1001
FOLDER = 1002


class QWC2TreeWidget(QTreeWidget):
    signal_drag_and_drop = pyqtSignal(str, str)

    def __init__(self, parent=None):
        """Dialog to load list and load an existing project into QWC2"""
        # init module and ui
        super().__init__(parent)
        self.network_manager = NetworkRequestsManager.instance()
        self.list_project = []
        self.folder_open_status = {}
        self.refresh(PlgOptionsManager.get_plg_settings().last_tenant)

        self.setDragEnabled(True)
        self.setAcceptDrops(True)
        self.setDropIndicatorShown(True)
        self.setDragDropMode(QTreeWidget.InternalMove)
        self.setSelectionMode(self.SingleSelection)
        self.viewport().setAcceptDrops(True)

    def dropEvent(self, event) -> None:
        """Event that occurs when you drag and drop. A signal is emitted to indicate the
        origin and destination of the drag and drop.
        """
        dragged_item = self.selectedItems()[0]
        new_item = self.itemAt(event.pos())

        if dragged_item and new_item:
            old_path = self.get_full_path(dragged_item)
            new_path = f"{self.get_full_path(new_item)}/{old_path.split('/')[-1]}"
            self.signal_drag_and_drop.emit(old_path, new_path)
            super().dropEvent(event)

        else:
            event.ignore()

    def dragMoveEvent(self, event):
        """Default dragMoveEvent behavior"""
        event.acceptProposedAction()
        super().dragMoveEvent(event)

    def get_full_path(self, item: QTreeWidgetItem) -> str:
        """Returns the path of a QTreeWidgetItem recursively back to the tree root.

        :param item: QTreeWidgetItem
        :type item: QTreeWidgetItem
        :return: Tree path
        :rtype: str
        """
        full_path = item.text(0)
        parent = item.parent()
        while parent:
            full_path = parent.text(0) + "/" + full_path
            parent = parent.parent()
        return full_path

    def update_projects_list(self, tenant_key) -> list[str]:
        """Returns the list of projects for the QWC2 instance

        :param tenant_key: Name of the tenant
        :type tenant_key: str
        :return: List of projets
        :rtype: list
        """
        if len(PlgOptionsManager.get_plg_settings().tenant_list) == 0:
            PlgLogger.log(
                message=self.tr(
                    "No QWC instance configuration is entered in the plugin parameters."
                ),
                log_level=Qgis.Info,
                duration=10,
                push=True,
            )
            self.list_project = []
            return
        try:
            tenant = Tenant(
                **PlgOptionsManager.get_plg_settings().tenant_list[tenant_key]
            )
            url_publish = tenant.url_publish
            req_result = self.network_manager.get_url(
                url=QUrl(f"{url_publish}/listprojectdir"), tenant_key=tenant_key
            )
        except:
            PlgLogger.log(
                message=self.tr(
                    "Impossible to connect QWC2 instance, please check settings"
                ),
                log_level=Qgis.Critical,
                duration=10,
                push=True,
            )
            return
        try:
            self.list_project = json.loads(req_result.data().decode())
        except Exception:
            PlgLogger.log(
                message=self.tr(
                    "Impossible to connect QWC2 instance, please check URL"
                ),
                log_level=Qgis.Critical,
                duration=10,
                push=True,
            )
            self.list_project = []

    def build_folder_recursively(self, folder) -> QTreeWidgetItem:
        """Recursive function that populates the tree from the json returned by the api

        :param folder: folder path
        :type folder: str
        :return: New item
        :rtype: QTreeWidgetItem
        """
        item = QTreeWidgetItem([folder["name"]], type=FOLDER)
        item.setFont(0, QFont("Helvetica", 10, QFont.Bold))
        item.setIcon(0, QIcon(QgsApplication.iconPath("mIconFolder24.svg")))
        folder_list = folder["folders"]
        folder_list = sorted(folder_list, key=lambda x: x["name"])
        for subfolder in folder_list:
            item.addChild(self.build_folder_recursively(subfolder))

        list_project = folder["projects"]
        list_project.sort()
        for projet in list_project:
            child = QTreeWidgetItem([projet, folder["name"]], type=FILE)
            child.setIcon(0, QIcon(QgsApplication.iconPath("mIconQgsProjectFile.svg")))
            item.addChild(child)
        return item

    def refresh(self, tenant_key) -> None:
        """Create or re-create the tree from the list of returned projects."""
        self.save_folder_open_status()
        self.update_projects_list(tenant_key)
        self.clear()

        if len(self.list_project) == 0:
            return

        item = QTreeWidgetItem([""], type=FOLDER)

        item.setFont(0, QFont("Helvetica", 10, QFont.Bold))
        item.setIcon(0, QIcon(QgsApplication.iconPath("mIconFolder24.svg")))

        root_folder_list = self.list_project["folders"]
        root_folder_list = sorted(root_folder_list, key=lambda x: x["name"])

        for root_folder in root_folder_list:
            item.addChild(self.build_folder_recursively(root_folder))
        self.insertTopLevelItems(0, [item])
        item.setExpanded(True)

        root_project_list = self.list_project["projects"]
        root_project_list.sort()
        for root_project in root_project_list:
            root_item = QTreeWidgetItem([root_project], type=FILE)
            root_item.setIcon(
                0, QIcon(QgsApplication.iconPath("mIconQgsProjectFile.svg"))
            )
            item.addChild(root_item)

        self.restore_folder_open_status()

    @property
    def selected_item_path(self) -> str:
        """Return to project path

        :return: Project path
        :rtype: str
        """
        selected_item = self.selected_item
        if selected_item:
            path = [selected_item.text(0)]
            parent = selected_item.parent()
            while parent:
                path.insert(0, parent.text(0))
                parent = parent.parent()
            return "/".join(path).lstrip("/")

        return None

    @property
    def selected_item(self) -> str:
        """Return to project path

        :return: Project path
        :rtype: str
        """
        selected_items = self.selectedItems()
        if selected_items:
            selected_item = selected_items[0]

            return selected_item

        return None

    def expand_folder(
        self, expand: bool, folder: QTreeWidgetItem, recursive: bool = True
    ) -> None:
        """expand all the folders in QTreeWidget

        :param item: Tree Widget target
        :type item: QTreeWidget
        """
        folder.setExpanded(expand)
        if recursive:
            for i in range(folder.childCount()):
                child_item = folder.child(i)
                self.expand_folder(expand, child_item)

    def expand_all(self) -> None:
        """Browse all QTreeWidget and open all the folders"""
        for i in range(self.topLevelItemCount()):
            root_item = self.topLevelItem(i)
            self.expand_folder(True, root_item)

    def collapse_all(self) -> None:
        """Browse all QTreeWidget and close all the folders"""
        for i in range(self.topLevelItemCount()):
            root_item = self.topLevelItem(i)
            self.expand_folder(False, root_item)

    def save_folder_open_status(self) -> None:
        """
        Records the state of folder opening in the QTreeWidget.
        """

        def browse_items(item, path=""):
            for i in range(item.childCount()):
                child = item.child(i)
                child_path = path + "/" + child.text(0) if path else child.text(0)
                if child.childCount() > 0:
                    self.folder_open_status[child_path] = child.isExpanded()
                    browse_items(child, child_path)

        browse_items(self.invisibleRootItem())

    def restore_folder_open_status(self) -> None:
        """
        Restores the open state of folders in the QTreeWidget.
        """

        def browse_items(item, path=""):
            for i in range(item.childCount()):
                child = item.child(i)
                child_path = path + "/" + child.text(0) if path else child.text(0)
                if child.childCount() > 0 and child_path in self.folder_open_status:
                    child.setExpanded(self.folder_open_status[child_path])
                    browse_items(child, child_path)

        browse_items(self.invisibleRootItem())
