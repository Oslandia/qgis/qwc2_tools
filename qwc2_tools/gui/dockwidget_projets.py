import json
import mimetypes
import re
from codecs import encode
from datetime import datetime
from pathlib import Path
from tempfile import TemporaryDirectory

from qgis.core import QgsApplication, QgsProject
from qgis.gui import QgsGui
from qgis.PyQt import uic
from qgis.PyQt.Qt import (
    QFile,
    QHBoxLayout,
    QLabel,
    QLineEdit,
    QMessageBox,
    QPushButton,
    QUrl,
    QVBoxLayout,
)
from qgis.PyQt.QtCore import QByteArray, QFileInfo, QIODevice, Qt, QTemporaryDir
from qgis.PyQt.QtGui import QDesktopServices, QKeyEvent
from qgis.PyQt.QtWidgets import QDialog, QDockWidget, QFileDialog, QMenu
from qgis.utils import iface

from qwc2_tools.__about__ import __title__
from qwc2_tools.gui.qwc2_treewidget import FILE, FOLDER
from qwc2_tools.toolbelt import PlgLogger
from qwc2_tools.toolbelt.log_handler import PlgLogger
from qwc2_tools.toolbelt.network_manager import NetworkRequestsManager
from qwc2_tools.toolbelt.preferences import PlgOptionsManager, Tenant


class DockWidgetProjets(QDockWidget):
    def __init__(self, parent=None):
        """Dialog to load list and load an existing project into QWC2"""
        # init module and ui
        super().__init__(parent)
        uic.loadUi(Path(__file__).parent / f"{Path(__file__).stem}.ui", self)
        self.network_manager = NetworkRequestsManager.instance()
        self._extension = None
        self.log = PlgLogger().log

        instance_gui = QgsGui.instance()
        instance_gui.optionsChanged.connect(self.onConfigUpdated)

        # Buttons icons
        self.dlg_btn_open_project.setIcon(
            QgsApplication.getThemeIcon("mIconFolderProject.svg")
        )
        self.dlg_btn_delete_project_folder.setIcon(
            QgsApplication.getThemeIcon("mIconDelete.svg")
        )
        self.dlg_btn_refresh.setIcon(QgsApplication.getThemeIcon("mActionRefresh.svg"))
        self.dlg_btn_create_folder.setIcon(
            QgsApplication.getThemeIcon("mActionNewFolder.svg")
        )
        self.dlg_btn_expand.setIcon(
            QgsApplication.getThemeIcon("mActionExpandNewTree.svg")
        )
        self.dlg_btn_new.setIcon(QgsApplication.getThemeIcon("mIconQgsProjectFile.svg"))
        self.dlg_btn_update.setIcon(QgsApplication.getThemeIcon("mAddToProject.svg"))
        self.dlg_btn_config.setIcon(
            QgsApplication.getThemeIcon("processingAlgorithm.svg")
        )
        self.dlg_btn_collapse.setIcon(
            QgsApplication.getThemeIcon("mActionCollapseTree.svg")
        )
        self.dlg_btn_web.setIcon(QgsApplication.getThemeIcon("mIconAms.svg"))

        # Connect buttons
        self.dlg_btn_refresh.clicked.connect(lambda: self.refresh())
        self.dlg_btn_open_project.clicked.connect(self.open_project)
        self.dlg_btn_delete_project_folder.clicked.connect(self.delete_project_folder)
        self.dlg_btn_expand.clicked.connect(self.the_tree.expand_all)
        self.dlg_btn_collapse.clicked.connect(self.the_tree.collapse_all)
        self.dlg_btn_create_folder.clicked.connect(self.create_folder)
        self.the_tree.itemSelectionChanged.connect(self.buttons_behavior)
        self.dlg_btn_new.clicked.connect(self.publish_new_project)
        self.dlg_btn_update.clicked.connect(self.update_project)
        self.dlg_btn_config.clicked.connect(self.open_settings)
        self.dlg_btn_web.clicked.connect(self.open_projet_on_qwc_web)

        # initial behaviour
        self.dlg_btn_open_project.setEnabled(False)
        self.dlg_btn_create_folder.setEnabled(False)
        self.dlg_btn_delete_project_folder.setEnabled(False)
        self.dlg_btn_update.setEnabled(False)
        self.dlg_btn_web.setEnabled(False)

        # Add tooltip to buttons
        self.dlg_btn_expand.setToolTip(self.tr("Expand all"))
        self.dlg_btn_collapse.setToolTip(self.tr("Collapse all"))
        self.dlg_btn_refresh.setToolTip(self.tr("Refresh"))
        self.dlg_btn_open_project.setToolTip(
            self.tr("Open the selected project (Enter)")
        )
        self.dlg_btn_delete_project_folder.setToolTip(
            self.tr("Delete selected folder or project (Del)")
        )
        self.dlg_btn_new.setToolTip(self.tr("Publish current project"))
        self.dlg_btn_update.setToolTip(
            self.tr("Update the selected project with the current project")
        )
        self.dlg_btn_create_folder.setToolTip(
            self.tr("Create a new folder in the selected folder")
        )
        self.dlg_btn_config.setToolTip(self.tr("Open plugin settings"))
        self.dlg_btn_web.setToolTip(self.tr("Open project in Web browser"))

        # Signal drag and drop
        self.the_tree.signal_drag_and_drop.connect(
            lambda *args: self.drag_and_drop(args[0], args[1])
        )

        # Double click to open
        self.the_tree.itemDoubleClicked.connect(self.open_project)

        # Right click
        self.the_tree.setContextMenuPolicy(Qt.CustomContextMenu)
        self.the_tree.customContextMenuRequested.connect(self.on_context_menu)

        # Tenant combobox
        self.tenant_cbb.currentTextChanged.connect(self.change_tenant_cbb)
        self.update_tenant_cbb()

    def onConfigUpdated(self) -> None:
        """Action to take when config is updated from settings."""
        self.update_tenant_cbb()
        self.refresh()
        if self.tenant_cbb.currentText() == "":
            self.the_tree.clear()

    @property
    def extension(self):
        """Get project extension, run api if variable is None"""
        if not self._extension:
            url_publish = self.get_tenant.url_publish
            response = self.network_manager.get_url(
                url=QUrl(f"{url_publish}/getprojecttype"),
                tenant_key=self.tenant_cbb.currentText(),
            )
            if not response:
                self._extension = None
            else:
                self._extension = (
                    "." + json.loads(response.data().decode("utf-8"))["type"]
                )

        return self._extension

    def update_tenant_cbb(self):
        self.tenant_cbb.blockSignals(True)
        self.tenant_cbb.clear()
        self.tenant_cbb.blockSignals(False)
        last_tenant = PlgOptionsManager.get_plg_settings().last_tenant
        list_tenant = list(PlgOptionsManager.get_plg_settings().tenant_list.keys())

        if last_tenant in list_tenant:
            self.tenant_cbb.blockSignals(True)
            self.tenant_cbb.addItems(list_tenant)
            self.tenant_cbb.blockSignals(False)
            self.tenant_cbb.setCurrentText(last_tenant)
        else:
            self.tenant_cbb.addItems(list_tenant)
        self.refresh()

    def button_behavior_during_refresh(self):
        if self.tenant_cbb.currentText() == "":
            self.dlg_btn_new.setEnabled(False)
        else:
            self.dlg_btn_new.setEnabled(True)

        # initial behaviour
        self.dlg_btn_open_project.setEnabled(False)
        self.dlg_btn_create_folder.setEnabled(False)
        self.dlg_btn_delete_project_folder.setEnabled(False)
        self.dlg_btn_update.setEnabled(False)
        self.dlg_btn_web.setEnabled(False)

    def change_tenant_cbb(self):
        PlgOptionsManager.set_value_from_key(
            "last_tenant", self.tenant_cbb.currentText()
        )
        PlgOptionsManager.set_value_from_key("dirty_authent", True)
        self.refresh()

    def keyPressEvent(self, event) -> None:
        """Event that occurs when the enter and delete keys are pressed to click on an item in the tree."""
        if event.key() == Qt.Key_Delete:
            self.delete_project_folder()
        elif event.key() == Qt.Key_Return or event.key() == Qt.Key_Enter:
            self.open_project()

    def open_settings(self):
        """Open settings window when clicking on the settings menu"""
        iface.showOptionsDialog(currentPage="mOptionsPage{}".format(__title__))

    def on_context_menu(self, point):
        """Context menu that opens when an item in the tree is right-clicked. It can be opened or deleted."""
        menu = QMenu(self.the_tree)
        delete = menu.addAction(self.tr("Delete"))
        delete.triggered.connect(self.delete_project_folder)
        if self.the_tree.selected_item.type() == FILE:
            open_project = menu.addAction(self.tr("Open on QGIS"))
            open_project.triggered.connect(self.open_project)
            open_project_qwc = menu.addAction(self.tr("Open on QWC"))
            open_project_qwc.triggered.connect(self.open_projet_on_qwc_web)
            open_project = menu.addAction(self.tr("Udpate"))
            open_project.triggered.connect(self.update_project)
        if self.the_tree.selected_item.type() == FOLDER:
            new_folder = menu.addAction(self.tr("Create folder"))
            new_folder.triggered.connect(self.create_folder)
            publish_project = menu.addAction(self.tr("Publish project"))
            publish_project.triggered.connect(self.publish_new_project)
        menu.exec_(self.the_tree.mapToGlobal(point))

    def buttons_behavior(self):
        """Check if a project is selected in the tree,
        if a project is selected it's possible to update this project so
        the button is unlock
        """
        if self.the_tree.selected_item:
            if self.the_tree.selected_item.type() == FILE:
                self.dlg_btn_create_folder.setEnabled(False)
                self.dlg_btn_open_project.setEnabled(True)
                self.dlg_btn_delete_project_folder.setEnabled(True)
                self.dlg_btn_update.setEnabled(True)
                self.dlg_btn_web.setEnabled(True)

            else:
                self.dlg_btn_open_project.setEnabled(False)
                self.dlg_btn_create_folder.setEnabled(True)
                self.dlg_btn_delete_project_folder.setEnabled(True)
                self.dlg_btn_update.setEnabled(False)
                self.dlg_btn_web.setEnabled(False)

    def refresh(self) -> None:
        """Refresh the tree"""
        self.the_tree.refresh(self.tenant_cbb.currentText())
        self.button_behavior_during_refresh()

    @property
    def get_tenant(self) -> Tenant:
        return Tenant(
            **PlgOptionsManager.get_plg_settings().tenant_list[
                self.tenant_cbb.currentText()
            ]
        )

    def build_web_url(self, project_path: str) -> str:
        """Build the project web url

        :param project_path: Project path
        :type project_path: str
        :return: Web URL
        :rtype: str
        """
        base_url = self.get_tenant.url_base
        return f"{base_url}/?t=scan/{project_path}"

    def open_projet_on_qwc_web(self) -> None:
        """Open qgis project on QWC web browser"""
        project_path = self.the_tree.selected_item_path.replace(self.extension, "")
        QDesktopServices.openUrl(QUrl(self.build_web_url(project_path)))

    def open_project(self) -> None:
        """Open the selected project in the current canvas"""
        if (
            not self.the_tree.selected_item_path
            or self.the_tree.selected_item.type() != FILE
        ):
            return

        result = iface.newProject(True)

        if not result:
            return

        project = QgsProject.instance()
        url_publish = self.get_tenant.url_publish
        url = f"{url_publish}/project/{self.the_tree.selected_item_path}"
        req_result = self.network_manager.get_url(
            url=QUrl(url), tenant_key=self.tenant_cbb.currentText()
        )

        # QGS Temporary file
        tmpdir = TemporaryDirectory(prefix="qwc2_tools_")
        qgs_path = Path(tmpdir.name).joinpath(
            f"projet_{datetime.now().strftime('%Y%m%d%H%M%S')}.qgs"
        )

        # Write xml project code to file
        qgs_path.write_text(req_result.data().decode())
        project.read(str(qgs_path))

    def delete_project_folder(self) -> None:
        """Delete the selected project"""

        url_publish = self.get_tenant.url_publish

        if not self.the_tree.selected_item_path:
            return

        if self.the_tree.selected_item.type() == FILE:
            url = f"{url_publish}/project/{self.the_tree.selected_item_path}"
            message = self.tr(
                "Are you sure you want to drop the " "project {} ?"
            ).format(self.the_tree.selected_item_path)

        elif self.the_tree.selected_item.type() == FOLDER:
            url = f"{url_publish}/folder/{self.the_tree.selected_item_path}"
            message = self.tr(
                "Are you sure you want to delete the folder {} "
                "and all the projects and subfolders it contains ?"
            ).format(self.the_tree.selected_item_path)

        response = QMessageBox().warning(
            self,
            self.tr("Suppression"),
            message,
            QMessageBox.Ok | QMessageBox.Cancel,
            QMessageBox.Cancel,
        )

        if response == QMessageBox.Ok:
            self.network_manager.delete_url(
                url=QUrl(url), tenant_key=self.tenant_cbb.currentText()
            )
            self.refresh()

        else:
            pass

    def check_name(self, dialog, input) -> None:
        """This method is used to check that a name does not contain any special characters or spaces. It is used for folder creation and project publication."""
        if re.match(r"^[a-zA-Z0-9_-]*$", input.text()):
            dialog.accept()
        else:
            self.log(
                message=self.tr(
                    "{} contains spaces or special characters witch are not allowed.".format(
                        input.text()
                    )
                ),
                log_level=1,
                push=False,
                duration=10,
            )
            QMessageBox.warning(
                dialog,
                self.tr("Invalid name"),
                self.tr(
                    "{} contains spaces or special characters witch are not allowed.".format(
                        input.text()
                    )
                ),
            )

    def create_folder(self) -> None:
        """Create a new folder in the tree. Display IHM to pout the new folder name"""
        url_publish = self.get_tenant.url_publish

        if self.the_tree.selected_item_path:
            if self.the_tree.selected_item.type() == FOLDER:
                base_folder = self.the_tree.selected_item_path + "/"
            else:
                base_folder = "/".join(self.the_tree.selected_item_path.split("/")[:-1])
        else:
            base_folder = ""

        folder_dialog = QDialog()
        folder_dialog.setWindowTitle(self.tr("Create Folder"))
        layout = QVBoxLayout()
        horizontal_layout = QHBoxLayout()
        label = QLabel(base_folder)
        horizontal_layout.addWidget(label)
        folder_name_input = QLineEdit()
        folder_name_input.setMinimumWidth(100)
        horizontal_layout.addWidget(folder_name_input)
        layout.addLayout(horizontal_layout)
        horizontal_layout2 = QHBoxLayout()
        confirm_button = QPushButton(self.tr("Create"))
        confirm_button.setIcon(QgsApplication.getThemeIcon("mActionNewFolder.svg"))
        confirm_button.setEnabled(False)
        folder_name_input.textChanged.connect(
            lambda: confirm_button.setEnabled(bool(folder_name_input.text()))
        )
        confirm_button.clicked.connect(
            lambda: self.check_name(folder_dialog, folder_name_input)
        )
        horizontal_layout2.addWidget(confirm_button)
        cancel_button = QPushButton(self.tr("Cancel"))
        cancel_button.setIcon(QgsApplication.getThemeIcon("mTaskCancel.svg"))
        cancel_button.clicked.connect(folder_dialog.reject)
        horizontal_layout2.addWidget(cancel_button)
        layout.addLayout(horizontal_layout2)
        folder_dialog.setLayout(layout)
        result = folder_dialog.exec_()

        if result == QDialog.Accepted:
            new_folder = base_folder + folder_name_input.text()
            data = QByteArray()
            data.append(f"name={new_folder}")
            url = f"{url_publish}/folder/"
            self.network_manager.post_url(
                url=QUrl(url), data=data, tenant_key=self.tenant_cbb.currentText()
            )
            self.refresh()

    def publish_new_project(self):
        """Upload the current project as a new project in the QWC2 instance. A popup is
        displayed in which you enter the name you want to give to the project
        """
        tmpdir = QTemporaryDir()
        tmpdir.setAutoRemove(False)
        qgs_path = Path(tmpdir.path()).joinpath(
            f"projet_{datetime.now().strftime('%Y%m%d%H%M%S')}{self.extension}"
        )
        project = QgsProject.instance()

        name = project.fileName()
        is_dirty = project.isDirty()

        project.write(str(qgs_path))

        project.setFileName(name)
        project.setDirty(is_dirty)

        if self.the_tree.selected_item:
            if self.the_tree.selected_item.type() == FOLDER:
                # If the selected folder is the root, no slash is required.
                if self.the_tree.selected_item_path == "":
                    base_folder = ""
                else:
                    base_folder = self.the_tree.selected_item_path + "/"
            else:
                base_folder = "/".join(self.the_tree.selected_item_path.split("/")[:-1])
        else:
            base_folder = ""

        folder_dialog = QDialog()
        folder_dialog.setWindowTitle(self.tr("Publish new project"))
        layout = QVBoxLayout()
        horizontal_layout = QHBoxLayout()
        label = QLabel(base_folder)
        horizontal_layout.addWidget(label)
        project_name_input = QLineEdit()
        project_name_input.setMinimumWidth(150)
        horizontal_layout.addWidget(project_name_input)
        layout.addLayout(horizontal_layout)
        horizontal_layout2 = QHBoxLayout()
        confirm_button = QPushButton(self.tr("Publish"))
        confirm_button.setEnabled(False)
        project_name_input.textChanged.connect(
            lambda: confirm_button.setEnabled(bool(project_name_input.text()))
        )
        confirm_button.setIcon(QgsApplication.getThemeIcon("mIconQgsProjectFile.svg"))
        confirm_button.clicked.connect(
            lambda: self.check_name(folder_dialog, project_name_input)
        )
        horizontal_layout2.addWidget(confirm_button)
        cancel_button = QPushButton(self.tr("Cancel"))
        cancel_button.setIcon(QgsApplication.getThemeIcon("mTaskCancel.svg"))
        cancel_button.clicked.connect(folder_dialog.reject)
        horizontal_layout2.addWidget(cancel_button)
        layout.addLayout(horizontal_layout2)
        folder_dialog.setLayout(layout)
        result = folder_dialog.exec_()

        if result == QDialog.Accepted:
            if project_name_input.text().endswith(self.extension):
                new_project = base_folder + project_name_input.text()
            else:
                new_project = base_folder + project_name_input.text() + self.extension

            fp = QFile(str(qgs_path))
            fp.open(QIODevice.ReadOnly)

            body_list = []
            boundary = "wL36Yn8afVp8Ag7AmP8qZ0SA4n1v9T"
            body_list.append(encode("--" + boundary))
            body_list.append(encode("Content-Disposition: form-data; name=filename;"))
            body_list.append(encode("Content-Type: {}".format("text/plain")))
            body_list.append(encode(""))
            body_list.append(encode(new_project))
            body_list.append(encode("--" + boundary))
            body_list.append(
                encode(
                    "Content-Disposition: form-data; name=file; filename={0}".format(
                        QFileInfo(fp).fileName()
                    )
                )
            )
            fileType = (
                mimetypes.guess_type(str(qgs_path))[0] or "application/octet-stream"
            )
            body_list.append(encode("Content-Type: {}".format(fileType)))
            body_list.append(encode(""))
            body_list.append(fp.readAll())
            body_list.append(encode("--" + boundary + "--"))
            body_list.append(encode(""))
            body = b"\r\n".join(body_list)

            headers = {
                b"Content-type": bytes(
                    "multipart/form-data; boundary={}".format(boundary), "utf8"
                ),
            }
            url_publish = self.get_tenant.url_publish
            url = f"{url_publish}/project/"
            result = self.network_manager.post_url(
                url=QUrl(url),
                data=body,
                headers=headers,
                tenant_key=self.tenant_cbb.currentText(),
            )
            if result is not None:
                self.refresh()
                project_path = new_project.replace(self.extension, "")
                web_url = self.build_web_url(project_path)
                self.log(
                    message=self.tr(
                        "The new project has been successfully published: <a href='{}'>{}</a>"
                    ).format(web_url, web_url),
                    log_level=3,
                    push=True,
                    duration=15,
                )

        tmpdir.remove()

    def update_project(self):
        """Update an existing project in QWC2 instance with the current project"""
        tmpdir = QTemporaryDir()
        tmpdir.setAutoRemove(False)
        qgs_path = Path(tmpdir.path()).joinpath(
            f"projet_{datetime.now().strftime('%Y%m%d%H%M%S')}{self.extension}"
        )
        project = QgsProject.instance()
        name = project.fileName()
        is_dirty = project.isDirty()

        project.write(str(qgs_path))

        project.setFileName(name)
        project.setDirty(is_dirty)

        update_project = self.the_tree.selected_item_path

        message = self.tr(
            "Are you sure you're replacing project {} with the current "
            "project content ?"
        ).format(update_project)

        response = QMessageBox().warning(
            self,
            self.tr("Update project"),
            message,
            QMessageBox.Ok | QMessageBox.Cancel,
            QMessageBox.Cancel,
        )

        if response == QMessageBox.Ok:
            fp = QFile(str(qgs_path))
            fp.open(QIODevice.ReadOnly)
            body_list = []
            boundary = "wL36Yn8afVp8Ag7AmP8qZ0SA4n1v9T"
            body_list.append(encode("--" + boundary))
            body_list.append(
                encode(
                    "Content-Disposition: form-data; name=file; filename={0}".format(
                        QFileInfo(fp).fileName()
                    )
                )
            )
            fileType = (
                mimetypes.guess_type(str(qgs_path))[0] or "application/octet-stream"
            )
            body_list.append(encode("Content-Type: {}".format(fileType)))
            body_list.append(encode(""))
            body_list.append(fp.readAll())
            body_list.append(encode("--" + boundary + "--"))
            body_list.append(encode(""))
            body = b"\r\n".join(body_list)
            headers = {
                b"Content-type": bytes(
                    "multipart/form-data; boundary={}".format(boundary), "utf8"
                ),
            }
            url_publish = self.get_tenant.url_publish

            url = f"{url_publish}/project/{update_project}"
            result = self.network_manager.put_url(
                url=QUrl(url),
                data=body,
                headers=headers,
                tenant_key=self.tenant_cbb.currentText(),
            )
            if result is not None:
                self.refresh()
                project_path = update_project.replace(self.extension, "")
                web_url = self.build_web_url(project_path)
                self.log(
                    message=self.tr(
                        "The project has been successfully updated: <a href='{}'>{}</a>"
                    ).format(web_url, web_url),
                    log_level=3,
                    push=True,
                    duration=15,
                )

        tmpdir.remove()

    def drag_and_drop(self, origin, destination):
        """Move project or folder with drag and drop"""
        data = QByteArray()
        data.append(f"origin={origin}&")
        data.append(f"destination={destination}")

        headers = {
            b"Content-type": bytes("application/x-www-form-urlencoded;", "utf8"),
        }

        url_publish = self.get_tenant.url_publish
        url = f"{url_publish}/move"
        response = self.network_manager.post_url(
            url=QUrl(url),
            data=data,
            headers=headers,
            tenant_key=self.tenant_cbb.currentText(),
        )

        self.refresh()

        if response == 0:
            self.log(
                message=self.tr("{} was successfully move to {}").format(
                    origin, destination
                ),
                log_level=3,
                push=True,
                duration=10,
            )

        else:
            self.log(
                message=self.tr("Failed move {} to {}").format(origin, destination),
                log_level=2,
                push=True,
                duration=10,
            )
