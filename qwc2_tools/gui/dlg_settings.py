#! python3  # noqa: E265

"""
    Plugin settings form integrated into QGIS 'Options' menu.
"""

from dataclasses import asdict

# standard
from functools import partial
from pathlib import Path

# PyQGIS
from qgis.core import QgsApplication, QgsAuthMethodConfig
from qgis.gui import QgsAuthConfigSelect, QgsOptionsPageWidget, QgsOptionsWidgetFactory
from qgis.PyQt import uic
from qgis.PyQt.Qt import QUrl
from qgis.PyQt.QtCore import QRegularExpression, QSize, Qt
from qgis.PyQt.QtGui import QDesktopServices, QIcon, QRegularExpressionValidator
from qgis.PyQt.QtWidgets import (
    QComboBox,
    QDialog,
    QDialogButtonBox,
    QGridLayout,
    QGroupBox,
    QHBoxLayout,
    QLabel,
    QLineEdit,
    QMessageBox,
    QSpinBox,
    QTableWidgetItem,
)

# project
from qwc2_tools.__about__ import (
    __icon_path__,
    __title__,
    __uri_homepage__,
    __uri_tracker__,
    __version__,
)
from qwc2_tools.toolbelt import PlgLogger, PlgOptionsManager
from qwc2_tools.toolbelt.preferences import PlgSettingsStructure, Tenant

# ############################################################################
# ########## Globals ###############
# ##################################

FORM_CLASS, _ = uic.loadUiType(
    Path(__file__).parent / "{}.ui".format(Path(__file__).stem)
)


# ############################################################################
# ########## Classes ###############
# ##################################


class ConfigOptionsPage(FORM_CLASS, QgsOptionsPageWidget):
    """Settings form embedded into QGIS 'options' menu."""

    def __init__(self, parent):
        super().__init__(parent)
        self.log = PlgLogger().log
        self.plg_settings = PlgOptionsManager()

        # load UI and set objectName
        self.setupUi(self)
        self.setObjectName("mOptionsPage{}".format(__title__))

        # header
        self.lbl_title.setText(f"{__title__} - Version {__version__}")

        # customization
        self.btn_help.setIcon(QIcon(QgsApplication.iconPath("mActionHelpContents.svg")))
        self.btn_help.pressed.connect(
            partial(QDesktopServices.openUrl, QUrl(__uri_homepage__))
        )

        self.btn_report.setIcon(
            QIcon(QgsApplication.iconPath("console/iconSyntaxErrorConsole.svg"))
        )
        self.btn_report.pressed.connect(
            partial(QDesktopServices.openUrl, QUrl(f"{__uri_tracker__}/new/choose"))
        )

        self.btn_reset.setIcon(QIcon(QgsApplication.iconPath("mActionUndo.svg")))
        self.btn_reset.pressed.connect(self.reset_settings)

        # load previously saved settings
        self.load_settings()

        # URL validator
        self._qreg_url = QRegularExpression(
            r"^https?://[-\w.:/]+",
            QRegularExpression.UseUnicodePropertiesOption,
        )

        self.edit_tenant.setIcon(
            QIcon(QgsApplication.iconPath("mActionToggleEditing.svg"))
        )
        self.add_tenant.setIcon(QIcon(QgsApplication.iconPath("symbologyAdd.svg")))
        self.drop_tenant.setIcon(QIcon(QgsApplication.iconPath("symbologyRemove.svg")))

        self.add_tenant.clicked.connect(self.tenant_dialog)
        self.edit_tenant.setEnabled(False)
        self.edit_tenant.clicked.connect(lambda: self.tenant_dialog(update=True))
        self.drop_tenant.setEnabled(False)
        self.drop_tenant.clicked.connect(self.drop_selected_item_in_tenant_list)
        self.table_tenant_list.itemSelectionChanged.connect(self.interface_behavior)
        self.table_tenant_list.itemDoubleClicked.connect(
            lambda: self.tenant_dialog(update=True)
        )
        self.table_tenant_list.setHorizontalHeaderLabels(["Name", "URL"])

    def interface_behavior(self):
        """Defines the behaviour of the interface according to the type of connection selected"""
        if self.get_tenant_name_of_selected_item():
            self.edit_tenant.setEnabled(True)
            self.drop_tenant.setEnabled(True)
        else:
            self.drop_tenant.setEnabled(False)
            self.edit_tenant.setEnabled(False)

    def tenant_pop_up_behavior(
        self, type_authent, authent, url_authent, token_validity
    ) -> None:
        """Behavior of add/update tenant pop up"""
        if type_authent.currentIndex() == 0:
            authent.setEnabled(False)
            url_authent.setEnabled(False)
            token_validity.setEnabled(False)
        else:
            authent.setEnabled(True)
            url_authent.setEnabled(True)
            token_validity.setEnabled(True)

    def check_information_and_tenant_name(
        self,
        dialog: QDialog,
        tenant_name,
        type_authent,
        authent,
        url_authent,
        url_base,
        url_publish,
        old_tenant_name,
    ) -> None:
        """Check configuration information"""
        if (
            not url_base.text()
            or not url_publish.text()
            or not tenant_name.text()
            or (
                type_authent.currentIndex() in [1, 2]
                and not url_authent.text()
                and not authent.configId()
            )
        ):
            QMessageBox.warning(
                dialog,
                self.tr("Incomplete form"),
                self.tr("One or more configuration parameters are missing."),
            )
            return

        tenant_name_text = tenant_name.text()
        if (
            tenant_name_text != old_tenant_name
            and tenant_name_text in self.get_list_of_regsiter_tenant
        ):
            QMessageBox.warning(
                dialog,
                self.tr("Name of instance configuration already used"),
                self.tr(
                    "The name {} is already used for another instance configuration, please choose another.".format(
                        tenant_name_text
                    )
                ),
            )
            return

        auth_manager = QgsApplication.authManager()
        conf = QgsAuthMethodConfig()
        auth_manager.loadAuthenticationConfig(authent.configId(), conf, True)
        data = conf.configMap()
        method = next(iter(data.keys()), None)

        if (type_authent.currentIndex() == 1 and method != "password") or (
            type_authent.currentIndex() == 2 and method != "oauth2config"
        ):
            QMessageBox.warning(
                dialog,
                self.tr("Incorrect authentication params"),
                self.tr(
                    "The selected authentication is not consistent with the desired authentication type. Please check the settings."
                ),
            )
            return

        dialog.accept()

    def tenant_dialog(self, update: bool = False) -> None:
        """Dialog for append or update a tenant configuration

        :param update: Is an update (In opposition to an append), defaults to False
        :type update: bool, optional
        """
        if update:
            name_dialog = self.tr("Update instance configuration")
        else:
            name_dialog = self.tr("Add instance configuration")
        settings = self.plg_settings.get_plg_settings()
        tenant_dialog = QDialog()
        tenant_dialog.setWindowTitle(name_dialog)
        tenant_dialog.resize(582, 326)
        gridLayout = QGridLayout(tenant_dialog)
        groupBox = QGroupBox(tenant_dialog)
        gridLayout_2 = QGridLayout(groupBox)
        horizontalLayout_5 = QHBoxLayout()
        label_4 = QLabel(groupBox)
        label_4.setMinimumSize(QSize(200, 0))
        label_4.setSizeIncrement(QSize(150, 0))
        label_4.setBaseSize(QSize(150, 0))
        horizontalLayout_5.addWidget(label_4)
        tenant_name = QLineEdit(groupBox)
        horizontalLayout_5.addWidget(tenant_name)
        gridLayout_2.addLayout(horizontalLayout_5, 0, 0, 1, 1)
        horizontalLayout_6 = QHBoxLayout()
        label_6 = QLabel(groupBox)
        label_6.setMinimumSize(QSize(200, 0))
        label_6.setSizeIncrement(QSize(150, 0))
        label_6.setBaseSize(QSize(150, 0))
        horizontalLayout_6.addWidget(label_6)
        url_base = QLineEdit(groupBox)
        horizontalLayout_6.addWidget(url_base)
        gridLayout_2.addLayout(horizontalLayout_6, 1, 0, 1, 1)
        horizontalLayout = QHBoxLayout()
        label = QLabel(groupBox)
        label.setMinimumSize(QSize(200, 0))
        label.setMaximumSize(QSize(150, 16777215))
        horizontalLayout.addWidget(label)
        type_authent = QComboBox(groupBox)
        type_authent.setMinimumSize(QSize(0, 0))
        type_authent.setMaximumSize(QSize(16777215, 16777215))
        type_authent.setBaseSize(QSize(0, 0))
        type_authent.setObjectName("type_authent")
        type_authent.addItem("")
        type_authent.addItem("")
        type_authent.addItem("")
        type_authent.setItemText(0, "No authentification")
        type_authent.setItemText(1, "Basic")
        type_authent.setItemText(2, "OIDC")
        type_authent.currentIndexChanged.connect(
            lambda: self.tenant_pop_up_behavior(
                type_authent, authent, url_authent, token_validity
            )
        )
        horizontalLayout.addWidget(type_authent)
        gridLayout_2.addLayout(horizontalLayout, 2, 0, 1, 1)
        horizontalLayout_2 = QHBoxLayout()
        label_2 = QLabel(groupBox)
        label_2.setMinimumSize(QSize(200, 0))
        label_2.setMaximumSize(QSize(150, 16777215))
        label_2.setBaseSize(QSize(150, 0))
        horizontalLayout_2.addWidget(label_2)
        url_publish = QLineEdit(groupBox)
        url_publish.setObjectName("url_publish")
        horizontalLayout_2.addWidget(url_publish)
        gridLayout_2.addLayout(horizontalLayout_2, 3, 0, 1, 1)
        horizontalLayout_3 = QHBoxLayout()
        label_3 = QLabel(groupBox)
        label_3.setMinimumSize(QSize(200, 0))
        label_3.setSizeIncrement(QSize(150, 0))
        label_3.setBaseSize(QSize(150, 0))
        horizontalLayout_3.addWidget(label_3)
        url_authent = QLineEdit(groupBox)
        url_authent.setObjectName("url_authent")
        horizontalLayout_3.addWidget(url_authent)
        gridLayout_2.addLayout(horizontalLayout_3, 4, 0, 1, 1)
        horizontalLayout_4 = QHBoxLayout()
        label_5 = QLabel(groupBox)
        label_5.setMinimumSize(QSize(200, 0))
        label_5.setMaximumSize(QSize(150, 16777215))
        label_5.setSizeIncrement(QSize(150, 0))
        horizontalLayout_4.addWidget(label_5)
        token_validity = QSpinBox(groupBox)
        token_validity.setMinimum(1)
        token_validity.setMaximum(999999999)
        token_validity.setProperty("value", 60)
        token_validity.setObjectName("token_validity")
        horizontalLayout_4.addWidget(token_validity)
        gridLayout_2.addLayout(horizontalLayout_4, 5, 0, 1, 1)
        authent = QgsAuthConfigSelect(groupBox)
        authent.setObjectName("authent")
        gridLayout_2.addWidget(authent, 6, 0, 1, 1)
        buttonBox = QDialogButtonBox(groupBox)
        buttonBox.setOrientation(Qt.Horizontal)
        buttonBox.setStandardButtons(QDialogButtonBox.Cancel | QDialogButtonBox.Ok)
        gridLayout_2.addWidget(buttonBox, 7, 0, 1, 1)
        gridLayout.addWidget(groupBox, 2, 0, 1, 1)
        old_tenant_name = None
        buttonBox.accepted.connect(
            lambda: self.check_information_and_tenant_name(
                tenant_dialog,
                tenant_name,
                type_authent,
                authent,
                url_authent,
                url_base,
                url_publish,
                old_tenant_name,
            )
        )

        url_base.setValidator(QRegularExpressionValidator(self._qreg_url))
        url_publish.setValidator(QRegularExpressionValidator(self._qreg_url))
        url_authent.setValidator(QRegularExpressionValidator(self._qreg_url))
        buttonBox.rejected.connect(tenant_dialog.reject)  # type: ignore
        groupBox.setTitle(self.tr("Authentification configuration"))
        label_4.setText(self.tr("Instance name"))
        label_6.setText(self.tr("Base URL"))
        url_base.setPlaceholderText("http://localhost:8088/tenant")
        tenant_name.setPlaceholderText("instance")
        label.setText(self.tr("Authentification service"))
        label_2.setText(self.tr("Publish endpoint URL"))
        url_publish.setText("/publish")
        label_3.setText(self.tr("Authentification URL"))
        url_authent.setPlaceholderText("http://localhost:8088/tenant/auth/login")
        label_5.setText(self.tr("Token validity (minutes)"))

        if update and self.get_tenant_name_of_selected_item():
            old_tenant_name = self.get_tenant_name_of_selected_item()
            tenant_name.setText(old_tenant_name)
            tenant_config = self.get_config_tenant_from_name(
                self.get_tenant_name_of_selected_item()
            )
            type_authent.setCurrentIndex(tenant_config.type_authent)
            url_base.setText(tenant_config.url_base)
            url_authent.setText(tenant_config.url_authent)
            url_publish.setText(
                tenant_config.url_publish[len(tenant_config.url_base) :]
            )
            authent.setConfigId(tenant_config.authent_id)
            token_validity.setValue(tenant_config.token_validity)
        # Init behavior
        self.tenant_pop_up_behavior(type_authent, authent, url_authent, token_validity)
        result = tenant_dialog.exec_()

        if result == QDialog.Accepted:
            if update:
                del settings.tenant_list[self.get_tenant_name_of_selected_item()]
                self.plg_settings.save_from_object(settings)
            tenant = Tenant(
                url_base=url_base.text().rstrip("/"),
                type_authent=type_authent.currentIndex(),
                url_publish=url_base.text().rstrip("/")
                + "/"
                + url_publish.text().strip("/"),
                url_authent=url_authent.text().rstrip("/"),
                authent_id=authent.configId(),
                token_validity=token_validity.value(),
            )
            name = tenant_name.text()
            settings.tenant_list[name] = asdict(tenant)
            self.plg_settings.save_from_object(settings)
            self.update_table_tenant()

    def update_table_tenant(self):
        """The list of tenant configurations is added to the table view"""
        self.table_tenant_list.setRowCount(0)
        for elem in self.get_list_of_regsiter_tenant:
            self.table_tenant_list.insertRow(0)
            self.table_tenant_list.setItem(0, 0, QTableWidgetItem(elem))
            self.table_tenant_list.setItem(
                0,
                1,
                QTableWidgetItem((self.get_config_tenant_from_name(elem)).url_base),
            )

    def get_tenant_name_of_selected_item(self) -> str:
        """Return the tenant name of selected item in table

        :return: Tenant name
        :rtype: str
        """
        selected_items = self.table_tenant_list.selectedItems()
        if selected_items:
            row = selected_items[0].row()
            name_item = self.table_tenant_list.item(row, 0)
            if name_item:
                name = name_item.text()
                return name
        return None

    def drop_selected_item_in_tenant_list(self) -> None:
        """Drop selected tenant in tenant list"""
        settings = self.plg_settings.get_plg_settings()
        if self.get_tenant_name_of_selected_item() in settings.tenant_list:
            del settings.tenant_list[self.get_tenant_name_of_selected_item()]
            self.plg_settings.save_from_object(settings)
        self.update_table_tenant()

    def apply(self):
        """Called to permanently apply the settings shown in the options page (e.g. \
        save them to QgsSettings objects). This is usually called when the options \
        dialog is accepted."""
        settings = self.plg_settings.get_plg_settings()

        # misc
        settings.debug_mode = self.opt_debug.isChecked()
        settings.version = __version__

        settings.dirty_authent = True

        # dump new settings into QgsSettings
        self.plg_settings.save_from_object(settings)

        if __debug__:
            self.log(
                message=self.tr("DEBUG - Settings successfully saved."),
                log_level=4,
            )

    @property
    def get_list_of_regsiter_tenant(self) -> list:
        """Returns a list of tenant names registered in the settings.

        :return: List opf tenant name
        :rtype: list
        """
        settings = self.plg_settings.get_plg_settings()
        return list(settings.tenant_list.keys())

    def get_config_tenant_from_name(self, tenant_name: str) -> Tenant:
        """Returns the dataclass Tenant with the requested name.

        :param tenant_name: Tenant name
        :type tenant_name: str
        :return: Tenant dataclass
        :rtype: Tenant
        """
        return Tenant(**PlgOptionsManager.get_plg_settings().tenant_list[tenant_name])

    def load_settings(self):
        """Load options from QgsSettings into UI form."""
        settings = self.plg_settings.get_plg_settings()

        # global
        self.opt_debug.setChecked(settings.debug_mode)
        self.lbl_version_saved_value.setText(settings.version)
        self.update_table_tenant()

    def reset_settings(self):
        """Reset settings to default values (set in preferences.py module)."""
        default_settings = PlgSettingsStructure()

        # dump default settings into QgsSettings
        self.plg_settings.save_from_object(default_settings)

        # update the form
        self.load_settings()


class PlgOptionsFactory(QgsOptionsWidgetFactory):
    """Factory for options widget."""

    def __init__(self):
        """Constructor."""
        super().__init__()

    def icon(self) -> QIcon:
        """Returns plugin icon, used to as tab icon in QGIS options tab widget.

        :return: _description_
        :rtype: QIcon
        """
        return QIcon(str(__icon_path__))

    def createWidget(self, parent) -> ConfigOptionsPage:
        """Create settings widget.

        :param parent: Qt parent where to include the options page.
        :type parent: QObject

        :return: options page for tab widget
        :rtype: ConfigOptionsPage
        """
        return ConfigOptionsPage(parent)

    def title(self) -> str:
        """Returns plugin title, used to name the tab in QGIS options tab widget.

        :return: plugin title from about module
        :rtype: str
        """
        return __title__

    def helpId(self) -> str:
        """Returns plugin help URL.

        :return: plugin homepage url from about module
        :rtype: str
        """
        return __uri_homepage__
