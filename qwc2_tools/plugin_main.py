#! python3  # noqa: E265

"""
    Main plugin module.
"""

# standard
from functools import partial
from pathlib import Path

# PyQGIS
from qgis.core import QgsApplication, QgsSettings
from qgis.gui import QgisInterface
from qgis.PyQt.QtCore import QCoreApplication, QLocale, Qt, QTranslator, QUrl
from qgis.PyQt.QtGui import QDesktopServices, QIcon
from qgis.PyQt.QtWidgets import QAction, QMenu
from qgis.utils import iface

# project
from qwc2_tools.__about__ import (
    DIR_PLUGIN_ROOT,
    __icon_path__,
    __title__,
    __uri_homepage__,
)
from qwc2_tools.gui.dlg_settings import PlgOptionsFactory
from qwc2_tools.gui.dockwidget_projets import DockWidgetProjets
from qwc2_tools.toolbelt import NetworkRequestsManager, PlgLogger

# ############################################################################
# ########## Classes ###############
# ##################################


class Qwc2Tools:
    def __init__(self, iface: QgisInterface):
        """Constructor.

        :param iface: An interface instance that will be passed to this class which \
        provides the hook by which you can manipulate the QGIS application at run time.
        :type iface: QgsInterface
        """
        self.iface = iface
        self.log = PlgLogger().log

        # translation
        # initialize the locale
        self.locale: str = QgsSettings().value("locale/userLocale", QLocale().name())[
            0:2
        ]
        locale_path: Path = (
            DIR_PLUGIN_ROOT / f"resources/i18n/{__title__.lower()}_{self.locale}.qm"
        )
        self.log(
            message=self.tr("Translation: {}, {}").format({self.locale}, {locale_path}),
            log_level=4,
        )
        if locale_path.exists():
            self.translator = QTranslator()
            self.translator.load(str(locale_path.resolve()))
            QCoreApplication.installTranslator(self.translator)

        self.menu_qwc2 = None
        self.dockwidget_visible = False
        self.dockwidget_projects = None

    def initGui(self):
        """Set up plugin UI elements."""

        # settings page within the QGIS preferences menu
        self.options_factory = PlgOptionsFactory()
        self.iface.registerOptionsWidgetFactory(self.options_factory)

        # Create instance of NetworkManager singleton
        NetworkRequestsManager()

        # -- Actions
        self.action_help = QAction(
            QgsApplication.getThemeIcon("mActionHelpContents.svg"),
            self.tr("Help"),
            self.iface.mainWindow(),
        )
        self.action_help.triggered.connect(
            partial(QDesktopServices.openUrl, QUrl(__uri_homepage__))
        )

        self.action_settings = QAction(
            QgsApplication.getThemeIcon("console/iconSettingsConsole.svg"),
            self.tr("Settings"),
            self.iface.mainWindow(),
        )
        self.action_settings.triggered.connect(
            lambda: self.iface.showOptionsDialog(
                currentPage="mOptionsPage{}".format(__title__)
            )
        )

        # -- Menu
        self.iface.addPluginToMenu(__title__, self.action_settings)
        self.iface.addPluginToMenu(__title__, self.action_help)

        # -- Help menu

        # documentation
        self.iface.pluginHelpMenu().addSeparator()
        self.action_help_plugin_menu_documentation = QAction(
            QIcon(str(__icon_path__)),
            f"{__title__} - Documentation",
            self.iface.mainWindow(),
        )
        self.action_help_plugin_menu_documentation.triggered.connect(
            partial(QDesktopServices.openUrl, QUrl(__uri_homepage__))
        )

        self.iface.pluginHelpMenu().addAction(
            self.action_help_plugin_menu_documentation
        )

        # Add QWC2 Menu
        self.menu_qwc2 = QMenu("QWC2", iface.mainWindow())
        self.menu_qwc2.setIcon(
            QIcon(
                str(DIR_PLUGIN_ROOT.joinpath("resources", "images", "default_icon.png"))
            )
        )
        iface.projectMenu().insertMenu(
            iface.projectMenu().actions()[13], self.menu_qwc2
        )
        # Config action
        self.add_action_to_menu(
            self.menu_qwc2, self.tr("Configure QWC2 Project Publisher"), "config.png"
        ).triggered.connect(self.open_settings)

        # Dockwidget projets
        self.add_action_to_menu(
            self.menu_qwc2, self.tr("Publish and manage projects"), "management.png"
        ).triggered.connect(self.show_dockwidget_projects)

        self.icon = QAction(
            QIcon(str(Path(DIR_PLUGIN_ROOT) / "resources/images/default_icon.png")),
            self.tr("QWC2 Tools"),
            self.iface.mainWindow(),
        )
        self.iface.addToolBarIcon(self.icon)
        self.icon.triggered.connect(self.click_icon)

    def add_action_to_menu(self, menu: QMenu, nom: str, icon: str):
        action = QAction(
            QIcon(str(DIR_PLUGIN_ROOT.joinpath("resources", "images", icon))),
            nom,
            iface.mainWindow(),
        )
        self.menu_qwc2.addAction(action)
        return action

    def tr(self, message: str) -> str:
        """Get the translation for a string using Qt translation API.

        :param message: string to be translated.
        :type message: str

        :returns: Translated version of message.
        :rtype: str
        """
        return QCoreApplication.translate(self.__class__.__name__, message)

    def unload(self):
        """Cleans up when plugin is disabled/uninstalled."""
        # -- Clean up menu
        self.iface.removePluginMenu(__title__, self.action_help)
        self.iface.removePluginMenu(__title__, self.action_settings)

        # -- Clean up preferences panel in QGIS settings
        self.iface.unregisterOptionsWidgetFactory(self.options_factory)

        # remove from QGIS help/extensions menu
        if self.action_help_plugin_menu_documentation:
            self.iface.pluginHelpMenu().removeAction(
                self.action_help_plugin_menu_documentation
            )

        # remove actions
        del self.action_settings
        del self.action_help

        if self.menu_qwc2:
            iface.projectMenu().removeAction(self.menu_qwc2.menuAction())
            self.menu_qwc2 = None

        self.iface.removeToolBarIcon(self.icon)
        if self.dockwidget_visible:
            self.iface.removeDockWidget(self.dockwidget_projects)
            self.dockwidget_visible = False

    def click_icon(self):
        if self.dockwidget_visible:
            self.iface.removeDockWidget(self.dockwidget_projects)
            self.dockwidget_visible = False
        else:
            self.show_dockwidget_projects()
            self.dockwidget_visible = True

    def show_dockwidget_projects(self):
        if not self.dockwidget_projects:
            self.dockwidget_projects = DockWidgetProjets()
        self.iface.addDockWidget(Qt.RightDockWidgetArea, self.dockwidget_projects)
        self.dockwidget_projects.show()
        self.dockwidget_visible = True

    def open_settings(self):
        """Open settings window when clicking on the settings menu"""
        self.iface.showOptionsDialog(currentPage="mOptionsPage{}".format(__title__))
