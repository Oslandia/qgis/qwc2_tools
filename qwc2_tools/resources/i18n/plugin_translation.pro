FORMS = ../../gui/dockwidget_projets.ui \
        ../../gui/dlg_settings.ui \

SOURCES= ../../plugin_main.py \
        ../../gui/dockwidget_projets.py \
        ../../gui/dlg_settings.py \
        ../../gui/qwc2_treewidget.py \
        ../../toolbelt/network_manager.py \

TRANSLATIONS = qwc2_tools_fr.ts
