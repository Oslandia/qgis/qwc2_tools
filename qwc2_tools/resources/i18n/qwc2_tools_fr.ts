<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR" sourcelanguage="en">
<context>
    <name>ConfigOptionsPage</name>
    <message>
        <location filename="../../gui/dlg_settings.py" line="420"/>
        <source>DEBUG - Settings successfully saved.</source>
        <translation>DEBUG - Paramètres sauvegardés avec succès.</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.py" line="320"/>
        <source>Authentification configuration</source>
        <translation>Configuration de l&apos;authentification</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.py" line="325"/>
        <source>Authentification service</source>
        <translation>Service d&apos;authentification</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.py" line="328"/>
        <source>Authentification URL</source>
        <translation>URL d&apos;authentification</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.py" line="330"/>
        <source>Token validity (minutes)</source>
        <translation>Validité du jeton (minutes)</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.py" line="172"/>
        <source>The name {} is already used for another instance configuration, please choose another.</source>
        <translation>Le nom {} est déja utilisé pour une autre instance, merci d&apos;en choisir un autre.</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.py" line="210"/>
        <source>Update instance configuration</source>
        <translation>Mise à jour d&apos;une configuration d&apos;instance</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.py" line="212"/>
        <source>Add instance configuration</source>
        <translation>Ajout d&apos;une configuration d&apos;instance</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.py" line="321"/>
        <source>Instance name</source>
        <translation>Nom de l&apos;instance</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.py" line="322"/>
        <source>Base URL</source>
        <translation>URL de base</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.py" line="160"/>
        <source>Incomplete form</source>
        <translation>Formulaire incomplet</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.py" line="160"/>
        <source>One or more configuration parameters are missing.</source>
        <translation>Une ou plusieurs informations de configuration sont manquantes.</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.py" line="172"/>
        <source>Name of instance configuration already used</source>
        <translation>Ce nom de configuration est déja utilisé</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.py" line="192"/>
        <source>Incorrect authentication params</source>
        <translation>Paramètres d&apos;authentification incorrects</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.py" line="192"/>
        <source>The selected authentication is not consistent with the desired authentication type. Please check the settings.</source>
        <translation>L&apos;authentification sélectionnée ne correspond pas au type d&apos;authentification souhaité. Veuillez vérifier les paramètres.</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.py" line="326"/>
        <source>Publish endpoint URL</source>
        <translation>Publish URL</translation>
    </message>
</context>
<context>
    <name>DockWidget</name>
    <message>
        <location filename="../../gui/dockwidget_projets.ui" line="135"/>
        <source>Projects</source>
        <translation>Projets</translation>
    </message>
    <message>
        <location filename="../../gui/dockwidget_projets.ui" line="14"/>
        <source>Browser QWC</source>
        <translation>Explorateur QWC</translation>
    </message>
    <message>
        <location filename="../../gui/dockwidget_projets.ui" line="173"/>
        <source>Instance :</source>
        <translation>Instance :</translation>
    </message>
</context>
<context>
    <name>DockWidgetProjets</name>
    <message>
        <location filename="../../gui/dockwidget_projets.py" line="93"/>
        <source>Expand all</source>
        <translation>Tout ouvrir</translation>
    </message>
    <message>
        <location filename="../../gui/dockwidget_projets.py" line="95"/>
        <source>Refresh</source>
        <translation>Rafraîchir</translation>
    </message>
    <message>
        <location filename="../../gui/dockwidget_projets.py" line="103"/>
        <source>Update the selected project with the current project</source>
        <translation>Mettre à jour le projet avec le contenu du projet courant</translation>
    </message>
    <message>
        <location filename="../../gui/dockwidget_projets.py" line="102"/>
        <source>Publish current project</source>
        <translation>Publier le projet</translation>
    </message>
    <message>
        <location filename="../../gui/dockwidget_projets.py" line="106"/>
        <source>Create a new folder in the selected folder</source>
        <translation>Créer un nouveau dossier dans le dossier sélectionné</translation>
    </message>
    <message>
        <location filename="../../gui/dockwidget_projets.py" line="308"/>
        <source>Are you sure you want to drop the project {} ?</source>
        <translation>Êtes-vous sûr de vouloir supprimer le projet {} ?</translation>
    </message>
    <message>
        <location filename="../../gui/dockwidget_projets.py" line="314"/>
        <source>Are you sure you want to delete the folder {} and all the projects and subfolders it contains ?</source>
        <translation>Êtes-vous sûr de vouloir supprimer le dossier {} et tous les projets et sous-dossiers qu&apos;il contient ?</translation>
    </message>
    <message>
        <location filename="../../gui/dockwidget_projets.py" line="319"/>
        <source>Suppression</source>
        <translation>Suppression</translation>
    </message>
    <message>
        <location filename="../../gui/dockwidget_projets.py" line="374"/>
        <source>Create Folder</source>
        <translation>Créer un dossier</translation>
    </message>
    <message>
        <location filename="../../gui/dockwidget_projets.py" line="384"/>
        <source>Create</source>
        <translation>Créer</translation>
    </message>
    <message>
        <location filename="../../gui/dockwidget_projets.py" line="464"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../../gui/dockwidget_projets.py" line="444"/>
        <source>Publish new project</source>
        <translation>Publier un nouveau projet</translation>
    </message>
    <message>
        <location filename="../../gui/dockwidget_projets.py" line="454"/>
        <source>Publish</source>
        <translation>Publier</translation>
    </message>
    <message>
        <location filename="../../gui/dockwidget_projets.py" line="502"/>
        <source>The new project has been successfully published: {}</source>
        <translation type="obsolete">Le nouveau projet a été publié avec succès {}</translation>
    </message>
    <message>
        <location filename="../../gui/dockwidget_projets.py" line="552"/>
        <source>Are you sure you&apos;re replacing project {} with the current project content ?</source>
        <translation>Êtes-vous sûr de remplacer projet {} par le contenu du projet actuel ?</translation>
    </message>
    <message>
        <location filename="../../gui/dockwidget_projets.py" line="557"/>
        <source>Update project</source>
        <translation>Mise à jour du projet</translation>
    </message>
    <message>
        <location filename="../../gui/dockwidget_projets.py" line="583"/>
        <source>The project {} has been successfully updated</source>
        <translation type="obsolete">Le projet {} a été mis à jour avec succès</translation>
    </message>
    <message>
        <location filename="../../gui/dockwidget_projets.py" line="109"/>
        <source>Open plugin settings</source>
        <translation>Ouvrir les paramètres du plugin</translation>
    </message>
    <message>
        <location filename="../../gui/dockwidget_projets.py" line="94"/>
        <source>Collapse all</source>
        <translation>Fermer tous les dossiers</translation>
    </message>
    <message>
        <location filename="../../gui/dockwidget_projets.py" line="203"/>
        <source>Delete</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="../../gui/dockwidget_projets.py" line="210"/>
        <source>Udpate</source>
        <translation>Mettre à jour</translation>
    </message>
    <message>
        <location filename="../../gui/dockwidget_projets.py" line="213"/>
        <source>Create folder</source>
        <translation>Créer un dossier</translation>
    </message>
    <message>
        <location filename="../../gui/dockwidget_projets.py" line="215"/>
        <source>Publish project</source>
        <translation>Publier un projet</translation>
    </message>
    <message>
        <location filename="../../gui/dockwidget_projets.py" line="638"/>
        <source>{} was successfully move to {}</source>
        <translation>{} a été déplacé avec succès vers {}</translation>
    </message>
    <message>
        <location filename="../../gui/dockwidget_projets.py" line="96"/>
        <source>Open the selected project (Enter)</source>
        <translation>Ouvrir le project sélectionné (Entrée)</translation>
    </message>
    <message>
        <location filename="../../gui/dockwidget_projets.py" line="99"/>
        <source>Delete selected folder or project (Del)</source>
        <translation>Supprimer le projet ou fichier sélectionné (Suppr)</translation>
    </message>
    <message>
        <location filename="../../gui/dockwidget_projets.py" line="648"/>
        <source>Failed move {} to {}</source>
        <translation>Échec du déplacement de {} vers {}</translation>
    </message>
    <message>
        <location filename="../../gui/dockwidget_projets.py" line="351"/>
        <source>{} contains spaces or special characters witch are not allowed.</source>
        <translation>{} contient des espaces ou caractères spéciaux qui ne sont pas autorisés.</translation>
    </message>
    <message>
        <location filename="../../gui/dockwidget_projets.py" line="351"/>
        <source>Invalid name</source>
        <translation>Nom invalid</translation>
    </message>
    <message>
        <location filename="../../gui/dockwidget_projets.py" line="206"/>
        <source>Open on QGIS</source>
        <translation>Ouvrir dans QGIS</translation>
    </message>
    <message>
        <location filename="../../gui/dockwidget_projets.py" line="208"/>
        <source>Open on QWC</source>
        <translation>Ouvrir dans QWC</translation>
    </message>
    <message>
        <location filename="../../gui/dockwidget_projets.py" line="110"/>
        <source>Open project in Web browser</source>
        <translation>Ouvrir le projet dans la navigateur web</translation>
    </message>
    <message>
        <location filename="../../gui/dockwidget_projets.py" line="523"/>
        <source>The new project has been successfully published: &lt;a href=&apos;{}&apos;&gt;{}&lt;/a&gt;</source>
        <translation>Le nouveau projet a été publié avec succès: &lt;a href=&apos;{}&apos;&gt;{}&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../../gui/dockwidget_projets.py" line="605"/>
        <source>The project has been successfully updated: &lt;a href=&apos;{}&apos;&gt;{}&lt;/a&gt;</source>
        <translation>Le projet a été mis à jour avec succès:: &lt;a href=&apos;{}&apos;&gt;{}&lt;/a&gt;</translation>
    </message>
</context>
<context>
    <name>NetworkRequestsManager</name>
    <message>
        <location filename="../../toolbelt/network_manager.py" line="100"/>
        <source>The selected authentication is not consistent with the desired authentication type. Please check the settings.</source>
        <translation>L&apos;authentification sélectionnée ne correspond pas au type d&apos;authentification souhaité. Veuillez vérifier les paramètres.</translation>
    </message>
    <message>
        <location filename="../../toolbelt/network_manager.py" line="110"/>
        <source>No authentication url set in parameters</source>
        <translation>Pas d&apos;url d&apos;authentification définie dans les paramètres</translation>
    </message>
    <message>
        <location filename="../../toolbelt/network_manager.py" line="118"/>
        <source>No authentication has been set in the parameters</source>
        <translation>Aucune authentification n&apos;a été définie dans les paramètres</translation>
    </message>
    <message>
        <location filename="../../toolbelt/network_manager.py" line="249"/>
        <source>Something went wrong during request preparation: {}</source>
        <translation>Un problème s&apos;est produit lors de la préparation de la demande : {}</translation>
    </message>
    <message>
        <location filename="../../toolbelt/network_manager.py" line="325"/>
        <source>GET response: {}</source>
        <translation>GET réponse: {}</translation>
    </message>
    <message>
        <location filename="../../toolbelt/network_manager.py" line="494"/>
        <source>Houston, we&apos;ve got a problem: {}</source>
        <translation>Une erreur s&apos;est produite: {}</translation>
    </message>
    <message>
        <location filename="../../toolbelt/network_manager.py" line="375"/>
        <source>DELETE response: {}</source>
        <translation>DELETE réponse: {}</translation>
    </message>
    <message>
        <location filename="../../toolbelt/network_manager.py" line="429"/>
        <source>POST response: {}</source>
        <translation>POST réponse: {}</translation>
    </message>
    <message>
        <location filename="../../toolbelt/network_manager.py" line="483"/>
        <source>PUT response: {}</source>
        <translation>PUT réponse: {}</translation>
    </message>
</context>
<context>
    <name>QWC2TreeWidget</name>
    <message>
        <location filename="../../gui/qwc2_treewidget.py" line="100"/>
        <source>Impossible to connect QWC2 instance, please check settings</source>
        <translation>Impossible de connecter l&apos;instance QWC2, veuillez vérifier les paramètres</translation>
    </message>
    <message>
        <location filename="../../gui/qwc2_treewidget.py" line="112"/>
        <source>Impossible to connect QWC2 instance, please check URL</source>
        <translation>Impossible de connecter l&apos;instance QWC2, veuillez vérifier l&apos;URL</translation>
    </message>
    <message>
        <location filename="../../gui/qwc2_treewidget.py" line="81"/>
        <source>No QWC instance configuration is entered in the plugin parameters.</source>
        <translation>Pas de configuration d&apos;instance QWC dans les paramètres du plugin.</translation>
    </message>
</context>
<context>
    <name>Qwc2Tools</name>
    <message>
        <location filename="../../plugin_main.py" line="54"/>
        <source>Translation: {}, {}</source>
        <translation>Traduction : {}, {}</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="78"/>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="87"/>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="130"/>
        <source>Configure QWC2 Project Publisher</source>
        <translation>Configuration de QWC2</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="135"/>
        <source>Publish and manage projects</source>
        <translation>Gérer et publier les projets</translation>
    </message>
    <message>
        <location filename="../../plugin_main.py" line="139"/>
        <source>QWC2 Tools</source>
        <translation>QWC2 Tools</translation>
    </message>
</context>
<context>
    <name>wdg_qwc2_tools_settings</name>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="14"/>
        <source>QWC2 Tools - Settings</source>
        <translation>QWC2 Tools - Paramètres</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="44"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;PluginTitle - Version X.X.X&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&amp;lt;html&amp;gt;&amp;lt;head/&amp;gt;&amp;lt;body&amp;gt;&amp;lt;p align=&amp;quot;center&amp;quot;&amp;gt;&amp;lt;span style=&amp;quot; font-weight:600;&amp;quot;&amp;gt;PluginTitle - Version X.X.X&amp;lt;/span&amp;gt;&amp;lt;/p&amp;gt;&amp;lt;/body&amp;gt;&amp;lt;/html&amp;gt;</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="75"/>
        <source>Miscellaneous</source>
        <translation>Divers</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="124"/>
        <source>Report an issue</source>
        <translation>Signaler un problème</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="146"/>
        <source>Version used to save settings:</source>
        <translation>Version utilisée pour sauvegarder les paramètres :</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="168"/>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="190"/>
        <source>Reset setttings to factory defaults</source>
        <translation>Réinitialiser les réglages aux valeurs par défaut</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="209"/>
        <source>Enable debug mode.</source>
        <translation>Activer le mode débogage.</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="218"/>
        <source>Debug mode (degraded performances)</source>
        <translation>Mode débogage (performances dégradées)</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="231"/>
        <source>Instance configuration</source>
        <translation>Configuration d&apos;instance</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="253"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="258"/>
        <source>Publish URL</source>
        <translation>URL de publication</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="282"/>
        <source>...</source>
        <translation></translation>
    </message>
</context>
</TS>
