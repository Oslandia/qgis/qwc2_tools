#! python3  # noqa: E265

"""
    Perform network request.
"""

# ############################################################################
# ########## Imports ###############
# ##################################


# Standard library
import logging
from datetime import datetime

# PyQGIS
from qgis.core import (
    QgsApplication,
    QgsAuthMethodConfig,
    QgsBlockingNetworkRequest,
    QgsNetworkAccessManager,
)
from qgis.PyQt.Qt import QByteArray, QEventLoop, QObject, QUrl, QUrlQuery
from qgis.PyQt.QtNetwork import QNetworkReply, QNetworkRequest

# project
from qwc2_tools.toolbelt.log_handler import PlgLogger
from qwc2_tools.toolbelt.preferences import PlgOptionsManager, Tenant

# ############################################################################
# ########## Globals ###############
# ##################################

logger = logging.getLogger(__name__)

# ############################################################################
# ########## Classes ###############
# ##################################


class Singleton(QObject):
    """
    Singleton
    """

    __instance = None

    @classmethod
    def instance(cls):
        """
        Récupère l'instance du singleton
        """
        return cls.__instance

    @classmethod
    def clearInstance(cls):
        """
        Nettoie l'instance du singleton
        """
        cls.__instance = None

    def __new__(cls, *args, **kwargs):
        if cls.__instance is None:
            cls.__instance = QObject.__new__(cls, *args, **kwargs)
        return cls.__instance


class NetworkRequestsManager(Singleton):
    """Helper on network operations."""

    def __init__(self, parent=None, useAuth=True):
        """Initialization."""
        super().__init__(parent)
        self.log = PlgLogger().log
        self.ntwk_requester = QgsBlockingNetworkRequest()
        self.manager = QgsNetworkAccessManager.instance()
        self.cookies = []
        self.crsf_token = None
        self.crsf_token_time = None

    def autent_required(self, tenant_key):
        tenant = Tenant(**PlgOptionsManager.get_plg_settings().tenant_list[tenant_key])
        return tenant.type_authent != 0

    def instance_authentification(self, tenant_key):
        """Authentication and token refresh management based on authentication type."""

        tenant = Tenant(**PlgOptionsManager.get_plg_settings().tenant_list[tenant_key])
        authent_id = tenant.authent_id

        auth_manager = QgsApplication.authManager()
        conf = QgsAuthMethodConfig()
        auth_manager.loadAuthenticationConfig(authent_id, conf, True)
        data = conf.configMap()
        method = next(iter(data.keys()), None)

        if (tenant.type_authent == 1 and method != "password") or (
            tenant.type_authent == 2 and method != "oauth2config"
        ):
            self.log(
                message=self.tr(
                    "The selected authentication is not consistent with the desired authentication type. Please check the settings."
                ),
                log_level=2,
                push=True,
            )
            return

        if not tenant.url_authent:
            self.log(
                message=self.tr("No authentication url set in parameters"),
                log_level=2,
                push=True,
            )
            return

        if not tenant.authent_id:
            self.log(
                message=self.tr("No authentication has been set in the parameters"),
                log_level=2,
                push=True,
            )
            return

        if self.update_crsf_token_needed(tenant_key):
            if tenant.type_authent == 1:
                self.basic_authentication(tenant_key)

            if tenant.type_authent == 2:
                self.oidc_authentification(tenant_key)

    def oidc_authentification(self, tenant_key):
        """Authentication for Open ID Connect"""
        tenant = Tenant(**PlgOptionsManager.get_plg_settings().tenant_list[tenant_key])
        url = QUrl(tenant.url_authent)
        authent_id = tenant.authent_id
        self.cookies = []
        self.crsf_token = None

        manager = QgsNetworkAccessManager.instance()
        auth_manager = QgsApplication.authManager()
        request = QNetworkRequest(url)
        request.setAttribute(QNetworkRequest.FollowRedirectsAttribute, False)
        auth_manager.updateNetworkRequest(request, authent_id)

        response = manager.get(request)

        loop = QEventLoop()
        response.finished.connect(loop.quit)
        loop.exec()

        cookies = self.manager.cookieJar().cookiesForUrl(url)

        for cookie in cookies:
            self.cookies.append(cookie)
            if cookie.name().data().decode("utf-8") == "csrf_access_token":
                self.crsf_token = cookie.value().data().decode("utf-8")
                self.crsf_token_time = datetime.now()

    def basic_authentication(self, tenant_key):
        """Authentication for basic authent (user / password)"""
        tenant = Tenant(**PlgOptionsManager.get_plg_settings().tenant_list[tenant_key])
        url = QUrl(tenant.url_authent)
        authent_id = tenant.authent_id

        self.cookies = []
        self.crsf_token = None

        auth_manager = QgsApplication.authManager()
        conf = QgsAuthMethodConfig()
        auth_manager.loadAuthenticationConfig(authent_id, conf, True)
        data = conf.configMap()

        params = QUrlQuery()
        params.addQueryItem("csrf_token", "")
        params.addQueryItem("username", data["username"])
        params.addQueryItem("password", data["password"])

        request = QNetworkRequest(url)
        request.setAttribute(QNetworkRequest.FollowRedirectsAttribute, True)
        response = self.manager.post(
            request, params.query(QUrl.FullyEncoded).encode("utf-8")
        )

        loop = QEventLoop()
        response.finished.connect(loop.quit)
        loop.exec()

        cookies = self.manager.cookieJar().cookiesForUrl(url)

        for cookie in cookies:
            self.cookies.append(cookie)
            if cookie.name().data().decode("utf-8") == "csrf_access_token":
                self.crsf_token = cookie.value().data().decode("utf-8")
                self.crsf_token_time = datetime.now()

    def update_crsf_token_needed(self, tenant_key):
        """Check if the crsf_token exist, check also if the time limit is exceeded
        and finally restarts autehntification if the configuration has been modified"""

        settings = PlgOptionsManager.get_plg_settings()
        tenant = Tenant(**PlgOptionsManager.get_plg_settings().tenant_list[tenant_key])
        token_validity = tenant.token_validity

        if not self.crsf_token or not self.crsf_token_time:
            return True

        if settings.dirty_authent:
            PlgOptionsManager.set_value_from_key("dirty_authent", False)
            return True

        delta = datetime.now() - self.crsf_token_time

        # Convert delta to minutes
        delta = delta.total_seconds() / 60

        if float(delta) > float(token_validity):
            return True

        return False

    def build_request(
        self,
        url: QUrl = None,
        headers: dict = None,
        type: str = None,
        tenant_key: str = None,
    ) -> QNetworkRequest:
        """Build request object using plugin settings.

        :return: network request object.
        :rtype: QNetworkRequest
        """
        # create network object
        qreq = QNetworkRequest(url=url)
        settings = PlgOptionsManager.get_plg_settings()

        # headers
        all_headers = {
            b"Accept": bytes("*/*", "utf8"),
            b"User-Agent": bytes(settings.http_user_agent, "utf8"),
        }
        if headers:
            all_headers.update(headers)

        # prepare request
        try:
            for k, v in all_headers.items():
                qreq.setRawHeader(k, v)
        except Exception as err:
            self.log(
                message=self.tr(
                    "Something went wrong during request preparation: {}"
                ).format(err),
                log_level=2,
                push=False,
            )
        if self.autent_required(tenant_key):
            qreq.setRawHeader(b"X-CSRF-Token", self.crsf_token.encode())

        if len(self.cookies):
            cookie_string = ""
            for cookie in self.cookies:
                cookie_string += (
                    str(cookie.name(), "utf-8")
                    + "="
                    + str(cookie.value(), "utf-8")
                    + ";"
                )
            cookie_string = cookie_string[:-1]
            qreq.setRawHeader(bytes("Cookie", "utf-8"), bytes(cookie_string, "utf-8"))

        if settings.debug_mode:
            self.log(
                message="{}: {}".format(type, qreq.url().toString()),
                log_level=4,
                push=False,
            )

        return qreq

    def get_url(
        self, url: QUrl = None, headers: dict = None, tenant_key: str = None
    ) -> QByteArray:
        """Send a get method.
        :raises ConnectionError: if any problem occurs during feed fetching.
        :raises TypeError: if response mime-type is not valid

        :return: feed response in bytes
        :rtype: QByteArray

        :example:

        .. code-block:: python

            import json
            response_as_dict = json.loads(str(response, "UTF8"))
        """
        if self.autent_required(tenant_key):
            self.instance_authentification(tenant_key)

        req = self.build_request(
            url=url, headers=headers, type="GET", tenant_key=tenant_key
        )

        # send request
        try:
            req_status = self.ntwk_requester.get(
                request=req,
                forceRefresh=True,
            )

            # check if request is fine
            if req_status != QgsBlockingNetworkRequest.NoError:
                self.log(
                    message=self.ntwk_requester.errorMessage(), log_level=2, push=1
                )
                raise ConnectionError(self.ntwk_requester.errorMessage())

            req_reply = self.ntwk_requester.reply()

            if req_reply.error() != QNetworkReply.NoError:
                self.log(message=req_reply.errorString(), log_level=2, push=1)
                raise ConnectionError(req_reply.errorString())

            if PlgOptionsManager.get_plg_settings().debug_mode:
                self.log(
                    message=self.tr("GET response: {}").format(
                        req_reply.content().data().decode("utf-8")
                    ),
                    log_level=4,
                    push=False,
                )

            return req_reply.content()

        except Exception as err:
            err_msg = self.tr("Houston, we've got a problem: {}".format(err))
            self.log(message=err_msg, log_level=2, push=1)

    def delete_url(
        self, url: QUrl = None, headers: dict = None, tenant_key: str = None
    ) -> QByteArray:
        """Send a delete method.
        :raises ConnectionError: if any problem occurs during feed fetching.
        :raises TypeError: if response mime-type is not valid

        :return: feed response in bytes
        :rtype: QByteArray
        """

        if self.autent_required(tenant_key):
            self.instance_authentification(tenant_key)

        req = self.build_request(
            url=url, headers=headers, type="DELETE", tenant_key=tenant_key
        )

        # send request
        try:
            req_status = self.ntwk_requester.deleteResource(request=req)

            # check if request is fine
            if req_status != QgsBlockingNetworkRequest.NoError:
                self.log(
                    message=self.ntwk_requester.errorMessage(), log_level=2, push=1
                )
                raise ConnectionError(self.ntwk_requester.errorMessage())

            req_reply = self.ntwk_requester.reply()

            if req_reply.error() != QNetworkReply.NoError:
                self.log(message=req_reply.errorString(), log_level=2, push=1)
                raise ConnectionError(req_reply.errorString())

            if PlgOptionsManager.get_plg_settings().debug_mode:
                self.log(
                    message=self.tr("DELETE response: {}").format(
                        req_reply.content().data().decode("utf-8")
                    ),
                    log_level=4,
                    push=False,
                )

            return req_reply.content()

        except Exception as err:
            err_msg = self.tr("Houston, we've got a problem: {}").format(err)
            self.log(message=err_msg, log_level=2, push=1)

    def post_url(
        self,
        url: QUrl = None,
        data: QByteArray = None,
        headers: dict = None,
        tenant_key: str = None,
    ) -> QByteArray:
        """Send a post method with data option.
        :raises ConnectionError: if any problem occurs during feed fetching.
        :raises TypeError: if response mime-type is not valid

        :return: feed response in bytes
        :rtype: QByteArray
        """

        if self.autent_required(tenant_key):
            self.instance_authentification(tenant_key)

        req = self.build_request(
            url=url, headers=headers, type="POST", tenant_key=tenant_key
        )

        # send request
        try:
            req_status = self.ntwk_requester.post(request=req, data=data)

            # check if request is fine
            if req_status != QgsBlockingNetworkRequest.NoError:
                self.log(
                    message=self.ntwk_requester.errorMessage(), log_level=2, push=1
                )
                raise ConnectionError(self.ntwk_requester.errorMessage())

            req_reply = self.ntwk_requester.reply()

            if req_reply.error() != QNetworkReply.NoError:
                self.log(message=req_reply.errorString(), log_level=2, push=1)
                raise ConnectionError(req_reply.errorString())

            if PlgOptionsManager.get_plg_settings().debug_mode:
                self.log(
                    message=self.tr("POST response: {}").format(
                        req_reply.content().data().decode("utf-8")
                    ),
                    log_level=4,
                    push=False,
                )

            return req_status

        except Exception as err:
            err_msg = self.tr("Houston, we've got a problem: {}").format(err)
            self.log(message=err_msg, log_level=2, push=1)

    def put_url(
        self,
        url: QUrl = None,
        data: QByteArray = None,
        headers: dict = None,
        tenant_key: str = None,
    ) -> QByteArray:
        """Send a post method with data option.
        :raises ConnectionError: if any problem occurs during feed fetching.
        :raises TypeError: if response mime-type is not valid

        :return: feed response in bytes
        :rtype: QByteArray
        """

        if self.autent_required(tenant_key):
            self.instance_authentification(tenant_key)

        req = self.build_request(
            url=url, headers=headers, type="PUT", tenant_key=tenant_key
        )

        # send request
        try:
            req_status = self.ntwk_requester.put(request=req, data=data)

            # check if request is fine
            if req_status != QgsBlockingNetworkRequest.NoError:
                self.log(
                    message=self.ntwk_requester.errorMessage(), log_level=2, push=1
                )
                raise ConnectionError(self.ntwk_requester.errorMessage())

            req_reply = self.ntwk_requester.reply()

            if req_reply.error() != QNetworkReply.NoError:
                self.log(message=req_reply.errorString(), log_level=2, push=1)
                raise ConnectionError(req_reply.errorString())

            if PlgOptionsManager.get_plg_settings().debug_mode:
                self.log(
                    message=self.tr("PUT response: {}").format(
                        req_reply.content().data().decode("utf-8")
                    ),
                    log_level=4,
                    push=False,
                )

            return req_reply.content()

        except Exception as err:
            err_msg = self.tr("Houston, we've got a problem: {}").format(err)
            self.log(message=err_msg, log_level=2, push=1)
