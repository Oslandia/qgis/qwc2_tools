# CHANGELOG

The format is based on [Keep a Changelog](https://keepachangelog.com/), and this project adheres to [Semantic Versioning](https://semver.org/).

<!--

Unreleased

## version_tag - YYYY-DD-mm

### Added

### Changed

### Removed

-->

## 1.4.2 - 2025-01-31

- Fix encoding_password : better way to pass data in basic auth post

## 1.4.1 - 2024-08-21

- Fix syntax and i18n !46

## 1.4.0 - 2024-08-20

- Do not create a folders and projets with spaces and/or special characters !39 !40
- Multi instance management directly from the plugin interface !41 !43
- Add the ability to open a project directly from the web (qwc app) !42 !44

## 1.3.0 - 2024-06-17

- After update a project, the open project remains the original one, not the one temporarily created for update. !38
- Warning nginx for projet size more than 1 mb. !37

## 1.2.0 - 2024-04-17

- After publishing a project, the open project remains the original one, not the one temporarily created for uploading. !34
- Fixed mismanagement of a failed publication. !35
- Fix bug authent : recalling the authent type parameter all the time, not just once in init !36

## 1.1.0 - 2024-04-08

- Fix instance change bug : change NetworkRequestsManager class to singleton. !33

## 1.0.1 - 2024-03-04

- Fix display condition after publication of a project. !30

## 1.0.0 - 2024-02-29

- First stable realease
- Plugin no longer tagged experimental
- Fix bug when double click on folder items on the tree !28
- In the explorer, projects and folders are sorted alphabetically. !29
- The project publication success log only appears if the instance conf has been refreshed. !30

## 0.6.2 - 2024-02-23

- Fix bug translation tooltip !27

## 0.6.1 - 2024-02-19

- Bug fix: Activate project update button and logs during update only if successful. !24

## 0.6.0 - 2024-02-15

- Better integration with QGIS, all management is done in a dockable explorer. !21
- Add icon to open and close this explorer. !21
- Small button above the explorer with tooltip. !21
- Drag and drop to move projects or folders !22
- Right-click action, double click and keyboard shortcuts !22
- Improved interface behavior during refreshes, with preservation of open folders. !22
- Suggest saving a project if it is not saved before opening a new one. !23

## 0.5.1-beta1 - 2023-11-11

- Fix an error, it is now mandatory to enter a name to publish a new project or create a folder !20

## 0.5.0-beta1 - 2023-11-27

- Added French translation of log messages and user interface !17
- Change the order in the tree, projects below folders

## 0.4.0-beta1 - 2023-11-24

- Add funding in documentation, about et readme !15
- Add OIDC (Oauth2) authentification method !14
- Enhanced manage and publish project dialog !12 !13

## 0.3.0-beta1 - 2023-11-23

- Bug fix: refreshing windows and configuration when there was a change !6
- Bug fix : Error with headers when there is no authentication, refactoring of headers for better management !7
- Add API log (request and response) when debug mode is activated !9
- Replacement of the project extension parameter with an api route that provides the information !10

## 0.2.1-beta1 - 2023-11-21

- CI/CD add job to publish the plugin to official qgis repository

## 0.2.0-beta1 - 2023-11-21

- Option to connect to the QWC2 instance in the parameters window
- Basic authentication management (user and password)
- Qgis integration: new entries in the project menu
- All management is done through a tree structure
- Delete/create folders
- Publish and update QGIS projects

## 0.1.0 - 2023-09-13

- First release
- Generated with the [QGIS Plugins templater](https://oslandia.gitlab.io/qgis/template-qgis-plugin/)
