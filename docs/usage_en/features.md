# Features

Once your plugin is properly configured, you can use it.

There are two ways to launch the QWC exporter:

- By clicking on the QWC icon in the QGIS toolbar
- By going to the Project menu and clicking on QWC2 > Publish and Manage Project

## Description of browser icons

![icons](../img/icons_browser.png)

In the order :

- [Open project](#open-project)
- [Create folder](#create-folder)
- [Publish current project](#publish-project)
- [Update](#update-project) a project with the contents of the current project
- Delete a [folder](#delete-project) or [project](#delete-folder)
- Refresh browser
- Close all tree folders
- Open all tree folders
- Access QWC instance parameters window

*Each button has a tooltip explaining its function.*

## Possible action in the browser

Project > QWC2 > Manage Projects

What you can do:

<a id="delete-project"></a>

### Delete a project

You can do this in 3 ways:

- Right-click on the project.
- With the dedicated button when the project is selected
- Press the DELETE key on the keyboard when the project is selected.

A window appears to confirm the operation.

<a id="delete-folder"></a>

![delete_project](../img/delete_project.gif)

### Delete a folder and the projects/subfolders it contains

You can do this in 3 ways:

- Right-click on the folder.
- With the dedicated button when the folder is selected
- Press the DELETE key on the keyboard when the folder is selected.

A window appears to confirm the operation.

![delete_folder](../img/delete_folder.gif)

<a id="create-folder"></a>

### Create a folder

Create a folder in the selected folder in the tree or at the root if the root is selected. The path of the new folder is then displayed in the confirmation popup.

You can create a folder by clicking on the dedicated button, or by right-clicking and then creating a folder.

![create_folder](../img/create_folder.gif)

<a id="publish-project"></a>

### Publish a new project

The content of the current project will be the new project that will be published. The project will be published in the selected folder in the tree. The popup will indicate the path of this new project and allow you to choose its name. You can publish a new project using the appropriate button, or by right-clicking on a folder.

![publish_project](../img/publish_project.gif)

:warning: If your QWC instance uses Nginx, you may encounter issues with "large" projects.

In Nginx, the default value is 1MB as seen in the [documentation](http://nginx.org/en/docs/http/ngx_http_core_module.html#client_max_body_size). Therefore, you will need to modify your Nginx configuration to increase this maximum in case you wish to publish a project that exceeds 1MB.

<a id="open-project"></a>

### Open project

There are 3 ways to open a QWC project in QGIS :

- Double-click on a project in the QWC explorer tree
- Right-click and open a project in the QWC explorer tree
- Select a project in the QWC explorer, then click on the associate button.
- Press the ENTER key on the keyboard when the project is selected.

![publish_project](../img/open_project.gif)

<a id="update-project"></a>

### Update an existing project

The content of the current project will replace the content of the project you have selected. Be cautious as during an update, the project retains the same name and location. Only the content changes. You can publish and update a project using the appropriate button, or by right-clicking on a folder.

![update_project](../img/update_project.gif)

### Move project or folder with drag and drop

You can drag and drop a project from one folder to another. Or even a folder and all the folders and/or projects it contains.

Please note that **if a project is defined in a theme, it cannot be moved**. QGIS will return an error message.

![move_folder_or_project](../img/drag_and_drop.gif)
