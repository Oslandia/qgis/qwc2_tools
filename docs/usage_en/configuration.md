# Configuration

## Prerequisites

⚠️ **Without these two prerequisites, the plugin will not be usable.**

### Running QWC Instance

For more information on installing QWC, refer to the [official documentation](https://qwc-services.github.io/) or the [GitHub repository](https://github.com/qgis/qwc).

### The `qwc-publish-service`

In this instance, you must have the project publishing service. Indeed, this plugin uses API routes from this service.
Therefore, it must be present in the QWC instance you are using.

You can find this service in this [repository](https://gitlab.com/Oslandia/qwc/qwc-publish-service).

## Plugin Configuration Window

Once the plugin is installed, you will have access to the plugin's configuration page. There are several ways to access it:

- Project > QWC > Configure QWC Project Publisher
- Settings > Options > QWC2_Tools

![](../img/settings.png)

## Parameters to Complete

### Authentication Mode

Three modes are available:

- No authentication
- Basic authentication (username and password)
- OpenID Connect authentication (via OAuth2)

Choose the one that suits your instance configuration.

⚠️ **For OIDC authentication**, your browser will open the first time and ask you to authenticate.

### Publication Service URL

⚠️ Fill in regardless of your authentication type. This is the URL of the project publishing service (see above).

Depending on how the service is configured, enter your URL. For example:

```
http://my_qwc_app/my_custom_tenant/publish
```

### Authentication URL

This is the URL that points to the authentication service. Only fill in if you have selected `OIDC` or `Basic`.

It may look like for basic authent:

```
http://my_qwc_app/my_custom_tenant/auth/login
```

And for OIDC :

```
http://my_qwc_app/my_custom_tenant/auth/tokenlogin
```

### Authentication Token Validity Duration

This is a duration in minutes.

This parameter indicates the validity duration of a token. For example, if you enter `60`, then 60 minutes after your first connection, the plugin will consider the token as expired and will initiate a new authentication to obtain a new token.

### Select Authentication

This parameter does not need to be filled for no authentication.

Finally, select the authentication that corresponds to the type of authentication you chose in the first parameter. If your authentication does not exist, you can create it directly from this interface by clicking on the :heavy_plus_sign: or edit an existing authentication by clicking on the :pencil2:

⚠️ Be sure to choose authentication that is consistent with the type:

For example, if I choose Basic authentication, do not select an OAuth2-type authentication (and vice versa).
