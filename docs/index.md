# {{ title }} - Documentation

> **Description:** {{ description }}  
> **Author and contributors:** {{ author }}  
> **Plugin version:** {{ version }}  
> **QGIS minimum version:** {{ qgis_version_min }}  
> **QGIS maximum version:** {{ qgis_version_max }}  
> **Source code:** {{ repo_url }}  
> **Last documentation update:** {{ date_update }}

----

```{toctree}
---
caption: User guide (English 🇬🇧)
maxdepth: 1
---
Installation <usage_en/installation>
Configuration <usage_en/configuration>
Features <usage_en/features>
```

```{toctree}
---
caption: Guide d'utilisation (Français 🇫🇷)
maxdepth: 1
---
Installation <usage_fr/installation>
Configuration <usage_fr/configuration>
Fonctionnalités <usage_fr/features>
```

```{toctree}
---
caption: Contribution guide
maxdepth: 1
---
development/contribute
development/environment
development/documentation
development/translation
development/packaging
development/testing
development/history
```

```{toctree}
---
caption: Miscellaneous
maxdepth: 1
---
misc/funding
```
