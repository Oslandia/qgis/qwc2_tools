# Configuration

## Prérequis

⚠️ **Sans ces deux prérequis, le plugin ne sera pas utilisable.**

### Instance QWC en cours d'exécution

Pour plus d'informations sur l'installation de QWC, consultez la [documentation officielle](https://qwc-services.github.io/) ou le [dépôt GitHub](https://github.com/qgis/qwc2).

### Service `qwc-publish-service`

Dans cette instance, vous devez disposer du service de publication de projet. En effet, ce plugin utilise des routes API de ce service.
Par conséquent, il doit être présent dans l'instance QWC que vous utilisez.

Vous pouvez trouver ce service dans ce [repertoire gitlab](https://gitlab.com/Oslandia/qwc/qwc-publish-service).

## Fenêtre de configuration du plugin

Une fois le plugin installé, vous aurez accès à la page de configuration du plugin. Il existe plusieurs façons d'y accéder :

- Projet > QWC > Configurer le projet QWC Publisher
- Paramètres > Options > QWC2_Tools

![](../img/settings.png)

## Paramètres à compléter

### Mode d'authentification

Trois modes sont disponibles :

- Aucune authentification
- Authentification de base (nom d'utilisateur et mot de passe)
- Authentification OpenID Connect (via OAuth2)

Choisissez celui qui convient à la configuration de votre instance.

⚠️ **Pour l'authentification OIDC**, votre navigateur s'ouvrira la première fois et vous demandera de vous authentifier.

### URL du service de publication

⚠️ Remplissez cette information indépendamment de votre type d'authentification. Il s'agit de l'URL du service de publication de projet (voir ci-dessus).

En fonction de la configuration du service, saisissez votre URL. Par exemple :

```
http://my_qwc_app/my_custom_tenant/publish
```

### URL d'authentification

Il s'agit de l'URL pointant vers le service d'authentification. Remplissez uniquement si vous avez sélectionné `OIDC` ou `Basique`.

Cela peut ressembler à ceci pour l'authentification de base :

```
http://my_qwc_app/my_custom_tenant/auth/login
```

Et pour l'OIDC :

```
http://my_qwc_app/my_custom_tenant/auth/tokenlogin
```

### Durée de validité du jeton d'authentification

Il s'agit d'une durée en minutes.

Ce paramètre indique la durée de validité d'un jeton. Par exemple, si vous saisissez `60`, alors 60 minutes après votre première connexion, le plugin considérera le jeton comme expiré et initiera une nouvelle authentification pour obtenir un nouveau jeton.

### Sélectionner l'authentification

Ce paramètre n'a pas besoin d'être rempli pour aucune authentification.

Enfin, sélectionnez l'authentification qui correspond au type d'authentification choisi dans le premier paramètre. Si votre authentification n'existe pas, vous pouvez la créer directement depuis cette interface en cliquant sur le :heavy_plus_sign: ou éditer une authentification existante en cliquant sur le :pencil2:

⚠️ Assurez-vous de choisir une authentification qui est cohérente avec le type :

Par exemple, si je choisis l'authentification de base, ne sélectionnez pas une authentification de type OAuth2 (et vice versa).
