# Installation

## Version stable (recommandée)

Ce plugin est publié sur le référentiel officiel des plugins QGIS : <https://plugins.qgis.org/plugins/qwc2_tools/>.

## Versions bêta publiées

Activez les extensions expérimentales dans le panneau de configuration du gestionnaire de plugins QGIS.

## Version de développement antérieure

Si vous vous considérez comme un adopteur précoce ou un testeur et que vous ne pouvez pas attendre la sortie officielle, le plugin est automatiquement empaqueté pour chaque commit sur la branche principale, vous pouvez donc utiliser cette adresse comme URL de référentiel dans les paramètres du gestionnaire d'extensions QGIS :

```url
https://oslandia.gitlab.io/qgis/qwc2_tools/plugins.xml
```

Soyez prudent, cette version peut être instable.
