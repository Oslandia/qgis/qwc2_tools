# Fonctionnalités

Une fois que votre plugin est correctement configuré, vous pouvez l'utiliser.

Il existe deux façons de lancer l'exportateur QWC :

- En cliquant sur l'icône QWC dans la barre d'outils de QGIS
- En allant dans le menu Projet et en cliquant sur QWC2 > Publier et gérer le projet

## Description des icônes du navigateur

![icônes](../img/icons_browser.png)

Dans l'ordre :

- [Ouvrir le projet](#open-project)
- [Créer un dossier](#create-folder)
- [Publier le projet actuel](#publish-project)
- [Mettre à jour un projet](#update-project) avec le contenu du projet actuel
- Supprimer un [dossier](#delete-folder) ou un [projet](#delete-project)
- Rafraîchir l'explorateur
- Fermer tous les dossiers de l'arborescence
- Ouvrir tous les dossiers de l'arborescence
- Accéder à la fenêtre des paramètres de l'instance QWC

*Chaque bouton a une info-bulle expliquant sa fonction.*

## Actions possibles dans le navigateur

Projet > QWC2 > Gérer les projets

Ce que vous pouvez faire :

<a id="delete-project"></a>

### Supprimer un projet

Vous pouvez le faire de 3 manières :

- Clic droit sur le projet.
- Avec le bouton dédié lorsque le projet est sélectionné
- Appuyez sur la touche SUPPR sur le clavier lorsque le projet est sélectionné.

Une fenêtre apparaît pour confirmer l'opération.

<a id="delete-folder"></a>

![supprimer_projet](../img/delete_project.gif)

### Supprimer un dossier et les projets/sous-dossiers qu'il contient

Vous pouvez le faire de 3 manières :

- Clic droit sur le dossier.
- Avec le bouton dédié lorsque le dossier est sélectionné
- Appuyez sur la touche SUPPR sur le clavier lorsque le dossier est sélectionné.

Une fenêtre apparaît pour confirmer l'opération.

![supprimer_dossier](../img/delete_folder.gif)

<a id="create-folder"></a>

### Créer un dossier

Créez un dossier dans le dossier sélectionné dans l'arborescence ou à la racine si la racine est sélectionnée. Le chemin du nouveau dossier est alors affiché dans la fenêtre contextuelle de confirmation.

Vous pouvez créer un dossier en cliquant sur le bouton dédié, ou en faisant un clic droit puis en créant un dossier.

![créer_dossier](../img/create_folder.gif)

<a id="publish-project"></a>

### Publier un nouveau projet

Le contenu du projet actuel sera le nouveau projet qui sera publié. Le projet sera publié dans le dossier sélectionné dans l'arborescence. La fenêtre contextuelle indiquera le chemin de ce nouveau projet et vous permettra de choisir son nom. Vous pouvez publier un nouveau projet en utilisant le bouton approprié, ou en cliquant avec le bouton droit sur un dossier.

![publier_projet](../img/publish_project.gif)

:warning: Attention, si votre instance QWC utilise Nginx il est possible que vous ayez un soucis avec les "gros" projets.

Dans Nginx, la valeur par défaut est à 1Mo comme on peux le voir dans le [documentation](http://nginx.org/en/docs/http/ngx_http_core_module.html#client_max_body_size). Il faudra alors modifier votre configuration Nginx pour augmenter ce maximum dans le cas ou vous souhaiteriez publier un projet qui dépasse 1Mo.

<a id="open-project"></a>

### Ouvrir un projet existant

Il existe 3 façons d'ouvrir un projet QWC dans QGIS :

- Double-cliquez sur un projet dans l'arborescence de l'explorateur QWC
- Clic droit et ouvrir un projet dans l'arborescence de l'explorateur QWC
- Sélectionnez un projet dans l'explorateur QWC, puis cliquez sur le bouton associé.
- Appuyez sur la touche ENTREE sur le clavier lorsque le projet est sélectionné.

![ouvrir_projet](../img/open_project.gif)

<a id="update-project"></a>

### Mettre à jour un projet existant

Le contenu du projet actuel remplacera le contenu du projet que vous avez sélectionné. Soyez prudent car lors d'une mise à jour, le projet conserve le même nom et emplacement. Seul le contenu change. Vous pouvez publier et mettre à jour un projet en utilisant le bouton approprié, ou en cliquant avec le bouton droit sur un dossier.

![mettre_à_jour_projet](../img/update_project.gif)

### Déplacer un projet ou un dossier par glisser-déposer

Vous pouvez faire glisser et déposer un projet d'un dossier à un autre. Ou même un dossier et tous les dossiers et/ou projets qu'il contient.

Veuillez noter que **si un projet est défini dans un thème, il ne peut pas être déplacé**. QGIS renverra un message d'erreur.

![déplacer_dossier_ou_projet](../img/drag_and_drop.gif)
